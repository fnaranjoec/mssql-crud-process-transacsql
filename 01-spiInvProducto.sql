SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Objeto:  StoredProcedure [dbo].[spiInvProducto]    Fecha de la secuencia de comandos: 03/15/2010 16:10:07 ******/
CREATE PROCEDURE [dbo].[spiInvProducto]
   @CCiProducto					nvarchar(20) = null
 , @CNoProducto					nvarchar(255) = null
 , @CCiCorto					nvarchar(10) = null
 , @CCiAlterno					nvarchar(20) = null
 , @CCiBarra					nvarchar(20) = null
 , @CCiAnterior					nvarchar(20) = null
 , @CCiReferencia				nvarchar(20) = null
 , @CDsProducto					nvarchar(max) = null
 , @CCeProducto					nchar(1) = null
 , @CSNInventariable			bit = 1
 , @CSNGrabaIva					bit = 1
 , @CSNValorizaIva				bit = 1
 , @CSNCompra					bit = 0
 , @CSNVenta					bit = 1
 , @CSNConsumo					bit = 0
 , @NVrPrecioPublico			decimal(18, 4) = 0
 , @NVrPrecioMayorista			decimal(18, 4) = 0
 , @NVrPrecioEspecial			decimal(18, 4) = 0
 , @NVrCostoPromedio			decimal(18, 4) = 0
 , @CCiClasificacion1			nchar(3) = null
 , @CNoClasificacion1			nvarchar(50) = null
 , @CCiClasificacion2			nchar(3) = null
 , @CNoClasificacion2			nvarchar(50) = null
 , @CCiClasificacion3			nchar(3) = null
 , @CNoClasificacion3			nvarchar(50) = null
 , @CCiTipoProducto				nchar(3) = null
 , @CNoTipoProducto				nvarchar(50) = null
 , @CCiProveedor				nvarchar(13) = null
 , @CNoProveedor				nvarchar(50) = null
 , @DFxRegistro					smalldatetime = null
 , @DFiIngreso					smalldatetime = null
 , @CCiUsuarioIngreso			nvarchar(20) = null
 , @CDsEstacionIngreso			nvarchar(20) = null
 , @DFmModifica					smalldatetime = null
 , @CCiUsuarioModifica			nvarchar(20) = null
 , @CDsEstacionModifica			nvarchar(20) = null
 , @NVrUltimoPrecioPublico		decimal(18, 4) = 0
 , @NVrUltimoPrecioMayorista	decimal(18, 4) = 0
 , @DFxUltimoPrecio				datetime = null
 , @NQnBalance					decimal(18, 4) = 0
 , @NVtBalance					decimal(18, 4) = 0
 , @CDsUnidad					nvarchar(10)=null
 , @NNuInnerBox					int=0
 , @NNuOuterBox					int=0
 , @NQnBalanceIni				decimal(18,4)=null
 , @NVtBalanceIni				decimal(18,4)=null
 , @NVrCostoPromedioIni			decimal(18,4)=null
 , @NVrCostoPromedioAnt			decimal(18,4)=null
 , @CCiPaisOrigen				nchar(3)=null
 , @CNoPaisOrigen				nvarchar(50)=null
 , @CCiCodigo					nchar(18)=null
 , @CNoAuxiliar					nvarchar(100)=null
 , @CCiCodigo2					nchar(18)=null
 , @CNoAuxiliar2				nvarchar(100)=null
 , @CDsPresentacion				nvarchar(50)=null
 , @OBjFoto						image=null
 , @CCiCodigo3					nchar(18)=null
 , @CNoAuxiliar3				nvarchar(100)=null
 , @NVrPrecioEspecial2			decimal(18, 4) = 0
 , @CDsUbicacion				nvarchar(20)=null
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 -- para uso del procedimiento
 -- Declare @NUMPEDIDO int
  Declare @Return_Value int

 -- para control de errores --
 Declare @ERR		int
 Declare @RC		int
 Declare @ROWS		int
 Declare @ERRTYPE	 nvarchar(20)
 Declare @ERRMESSAGE nvarchar(max)
 SET @ROWS=0

 /******************  ZONA DE ASERCION *********************/
 /* Validación de campos clave */
 Set @InfoMessage='Validando Clave Principal'
 Raiserror (@InfoMessage, 1, 1)
 If @CCiProducto is NULL
   Begin
     Raiserror ('Error en campo clave CCiProducto', 1, 1)
     Return 99999
   End
 Raiserror (@InfoMessage, 1, 5)

 /******************  ZONA DE EJECUCION *******************/
 /* ejecución del insert */
 Set @InfoMessage='Insertando Producto'
 Raiserror (@InfoMessage, 1, 5)
 INSERT
   TblInvProducto
 (
     CCiProducto
   , CNoProducto
   , CCiCorto
   , CCiAlterno
   , CCiBarra
   , CCiAnterior
   , CCiReferencia
   , CDsProducto
   , CCeProducto
   , CSNInventariable
   , CSNGrabaIva
   , CSNValorizaIva
   , CSNCompra
   , CSNVenta
   , CSNConsumo
   , NVrPrecioPublico
   , NVrPrecioMayorista
   , NVrPrecioEspecial
   , NVrCostoPromedio
   , CCiClasificacion1
   , CNoClasificacion1
   , CCiClasificacion2
   , CNoClasificacion2
   , CCiClasificacion3
   , CNoClasificacion3
   , CCiTipoProducto
   , CNoTipoProducto
   , CCiProveedor
   , CNoProveedor
   , DFxRegistro
   , CCiUsuarioIngreso
   , CDsEstacionIngreso
   , DFiIngreso
   , CCiUsuarioModifica
   , CDsEstacionModifica
   , DFmModifica
   , NVrUltimoPrecioPublico
   , NVrUltimoPrecioMayorista
   , DFxUltimoPrecio
   , NQnBalance
   , NVtBalance
   , CDsUnidad
   , NNuInnerBox
   , NNuOuterBox
   , NQnBalanceIni
   , NVtBalanceIni
   , NVrCostoPromedioIni
   , NVrCostoPromedioAnt
   , CCiPaisOrigen
   , CNoPaisOrigen
   , CCiCodigo
   , CNoAuxiliar
   , CCiCodigo2
   , CNoAuxiliar2
   , CDsPresentacion
   , OBjFoto
   , CCiCodigo3
   , CNoAuxiliar3
   , NVrPrecioEspecial2
   , CDsUbicacion
 )
VALUES
(
   @CCiProducto
 , @CNoProducto
 , @CCiCorto
 , @CCiAlterno
 , @CCiBarra
 , @CCiAnterior
 , @CCiReferencia
 , @CDsProducto
 , @CCeProducto
 , @CSNInventariable
 , @CSNGrabaIva
 , @CSNValorizaIva
 , @CSNCompra
 , @CSNVenta
 , @CSNConsumo
 , @NVrPrecioPublico
 , @NVrPrecioMayorista
 , @NVrPrecioEspecial
 , @NVrCostoPromedio
 , @CCiClasificacion1
 , @CNoClasificacion1
 , @CCiClasificacion2
 , @CNoClasificacion2
 , @CCiClasificacion3
 , @CNoClasificacion3
 , @CCiTipoProducto
 , @CNoTipoProducto
 , @CCiProveedor
 , @CNoProveedor
 , @DFxRegistro
 , @CCiUsuarioIngreso
 , @CDsEstacionIngreso
 , @DFiIngreso
 , @CCiUsuarioModifica
 , @CDsEstacionModifica
 , @DFmModifica
 , @NVrUltimoPrecioPublico
 , @NVrUltimoPrecioMayorista
 , @DFxUltimoPrecio
 , @NQnBalance
 , @NVtBalance
 , @CDsUnidad
 , @NNuInnerBox
 , @NNuOuterBox
 , @NQnBalanceIni
 , @NVtBalanceIni
 , @NVrCostoPromedioIni
 , @NVrCostoPromedioAnt
 , @CCiPaisOrigen
 , @CNoPaisOrigen
 , @CCiCodigo
 , @CNoAuxiliar
 , @CCiCodigo2
 , @CNoAuxiliar2
 , @CDsPresentacion
 , @OBjFoto
 , @CCiCodigo3
 , @CNoAuxiliar3
 , @NVrPrecioEspecial2
 , @CDsUbicacion
);

  Select @ERRMESSAGE='Eror al abrir Insertar el Producto.';
  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
  Raiserror (@InfoMessage, 1, 70)

--// *** GENERANDO BODEGAS ***
 Set @InfoMessage='Generando bodegas'
 Raiserror (@InfoMessage, 1, 70)
  EXEC @Return_Value=spiInvProductoBodegaGenerate @CCiProducto, @CDsUnidad
  If @Return_Value <> 0
    BEGIN
	  Select @ERRMESSAGE='Error al insertar las bodega y ubicaciones.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
    END

--// *** GENERANDO COMISIONES ***
 Set @InfoMessage='Insertando Comisiones'
 Raiserror (@InfoMessage, 1, 80)
  EXEC @Return_Value=spiInvProductoComisionGenerate @CCiProducto
  If @Return_Value <> 0
    BEGIN
	  Select @ERRMESSAGE='Error al insertar las comisiones del producto.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
    END

----// *** GENERANDO CLIENTES ***
-- Set @InfoMessage='Insertando Clientes'
-- Raiserror (@InfoMessage, 1, 80)
--  EXEC @Return_Value =[dbo].[spiInvProductoCliente] @CCiProducto, @NVrPrecioPublico, @NVrPrecioMayorista, @NVrPrecioEspecial;
--  If @Return_Value <> 0
--    BEGIN
--	  Select @ERRMESSAGE='Error al insertar los clientes del producto.';
--	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
--    END


  Raiserror (@InfoMessage, 1, 100)

  Raiserror ('Listo...', 1, 1)

  Return 0


ERR_HANDLER:
   RAISERROR(@ERRMESSAGE,11,1)
   RETURN @ERR
GO
