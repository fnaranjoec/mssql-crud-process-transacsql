SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spuInvProducto]
   @CCiProducto					nvarchar(20) = null
 , @CNoProducto					nvarchar(255) = null
 , @CCiCorto					nvarchar(10) = null
 , @CCiAlterno					nvarchar(20) = null
 , @CCiBarra					nvarchar(20) = null
 , @CCiAnterior					nvarchar(20) = null
 , @CCiReferencia				nvarchar(20) = null
 , @CDsProducto					nvarchar(max) = null
 , @CCeProducto					nchar(1) = null
 , @CSNInventariable			bit = 1
 , @CSNGrabaIva					bit = 1
 , @CSNValorizaIva				bit = 1
 , @CSNCompra					bit = 0
 , @CSNVenta					bit = 1
 , @CSNConsumo					bit = 0
 , @NVrPrecioPublico			decimal(18, 2) = 0
 , @NVrPrecioMayorista			decimal(18, 2) = 0
 , @NVrPrecioEspecial			decimal(18, 2) = 0
 , @NVrCostoPromedio			decimal(18, 2) = 0
 , @CCiClasificacion1			nchar(3) = null
 , @CNoClasificacion1			nvarchar(50) = null
 , @CCiClasificacion2			nchar(3) = null
 , @CNoClasificacion2			nvarchar(50) = null
 , @CCiClasificacion3			nchar(3) = null
 , @CNoClasificacion3			nvarchar(50) = null
 , @CCiTipoProducto				nchar(3) = null
 , @CNoTipoProducto				nvarchar(50) = null
 , @CCiProveedor				nvarchar(13) = null
 , @CNoProveedor				nvarchar(50) = null
 , @DFxRegistro					smalldatetime = null
 , @DFiIngreso					smalldatetime = null
 , @CCiUsuarioIngreso			nvarchar(20) = null
 , @CDsEstacionIngreso			nvarchar(20) = null
 , @DFmModifica					smalldatetime = null
 , @CCiUsuarioModifica			nvarchar(20) = null
 , @CDsEstacionModifica			nvarchar(20) = null
 , @NVrUltimoPrecioPublico		decimal(18, 4) = 0
 , @NVrUltimoPrecioMayorista	decimal(18, 4) = 0
 , @DFxUltimoPrecio				datetime = null
 , @NQnBalance					decimal(18, 4)=0
 , @NVtBalance					decimal(18, 4)=0
 , @CDsUnidad					nvarchar(10)
 , @NNuInnerBox					int=0
 , @NNuOuterBox					int=0
 , @NQnBalanceIni				decimal(18,4)=null
 , @NVtBalanceIni				decimal(18,4)=null
 , @NVrCostoPromedioIni			decimal(18,4)=null
 , @NVrCostoPromedioAnt			decimal(18,4)=null
 , @CCiPaisOrigen				nchar(3)=null
 , @CNoPaisOrigen				nvarchar(50)=null
 , @CCiCodigo					nchar(18)=null
 , @CNoAuxiliar					nvarchar(100)=null
 , @CCiCodigo2					nchar(18)=null
 , @CNoAuxiliar2				nvarchar(100)=null
 , @CDsPresentacion				nvarchar(50)=null
 , @OBjFoto						image=null
 , @CCiCodigo3					nchar(18)=null
 , @CNoAuxiliar3				nvarchar(100)=null
 , @NVrPrecioEspecial2			decimal(18, 4) = 0
 , @CDsUbicacion				nvarchar(20)=null

AS

 Declare @CNoProducto1				nvarchar(255)
 Declare @CCiCorto1					nvarchar(10)
 Declare @CCiAlterno1				nvarchar(20)
 Declare @CCiBarra1					nvarchar(20)
 Declare @CCiAnterior1				nvarchar(20)
 Declare @CCiReferencia1			nvarchar(20)
 Declare @CDsProducto1				nvarchar(max)
 Declare @CCeProducto1				nchar(1)
 Declare @CSNInventariable1			bit
 Declare @CSNGrabaIva1				bit
 Declare @CSNValorizaIva1			bit
 Declare @CSNCompra1				bit
 Declare @CSNVenta1					bit
 Declare @CSNConsumo1				bit
 Declare @NVrPrecioPublico1			decimal(18, 2)
 Declare @NVrPrecioMayorista1		decimal(18, 2)
 Declare @NVrPrecioEspecial1		decimal(18, 2)
 Declare @NVrCostoPromedio1			decimal(18, 2)
 Declare @CCiClasificacion11		nchar(3)
 Declare @CNoClasificacion11		nvarchar(50)
 Declare @CCiClasificacion21		nchar(3)
 Declare @CNoClasificacion21		nvarchar(50)
 Declare @CCiClasificacion31		nchar(3)
 Declare @CNoClasificacion31		nvarchar(50)
 Declare @CCiTipoProducto1			nchar(3)
 Declare @CNoTipoProducto1			nvarchar(50)
 Declare @CCiProveedor1				nvarchar(13)
 Declare @CNoProveedor1				nvarchar(50)
 Declare @DFxRegistro1				smalldatetime
 Declare @DFiIngreso1				smalldatetime
 Declare @CCiUsuarioIngreso1		nvarchar(20)
 Declare @CDsEstacionIngreso1		nvarchar(20)
 Declare @DFmModifica1				smalldatetime
 Declare @CCiUsuarioModifica1		nvarchar(20)
 Declare @CDsEstacionModifica1		nvarchar(20)
 Declare @NVrUltimoPrecioPublico1   decimal(18, 4)
 Declare @NVrUltimoPrecioMayorista1 decimal(18, 4)
 Declare @DFxUltimoPrecio1			datetime
 Declare @NQnBalance1				decimal(18, 4)
 Declare @NVtBalance1				decimal(18, 4)
 Declare @CDsUnidad1				nvarchar(10)
 Declare @NNuInnerBox1				int
 Declare @NNuOuterBox1				int
 Declare @NQnBalanceIni1			decimal(18,4)=null
 Declare @NVtBalanceIni1			decimal(18,4)=null
 Declare @NVrCostoPromedioIni1		decimal(18,4)=null
 Declare @NVrCostoPromedioAnt1		decimal(18,4)=null
 Declare @CCiPaisOrigen1			nchar(3)
 Declare @CNoPaisOrigen1			nvarchar(50)
 Declare @CCiCodigo1				nchar(18)
 Declare @CNoAuxiliar1				nvarchar(100)
 Declare @CCiCodigo21				nchar(18)
 Declare @CNoAuxiliar21				nvarchar(100)
 Declare @CDsPresentacion1			nvarchar(50)
 Declare @CCiCodigo31				nchar(18)
 Declare @CNoAuxiliar31				nvarchar(100)
 Declare @NVrPrecioEspecial21		decimal(18, 4)
 Declare @CDsUbicacion1				nvarchar(20)


 SET NOCOUNT ON;
 SET XACT_ABORT ON

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 -- para control de errores --
 Declare @ERR		int
 Declare @RC		int
 Declare @ROWS		int
 Declare @ERRTYPE	 nvarchar(20)
 Declare @ERRMESSAGE nvarchar(max)
 SET @ROWS=0

/********************** ZONA DE ASERCION *********************************/
 /* Validación de campos clave */
Set @InfoMessage='Validando Clave Principal'
Raiserror (@InfoMessage, 1, 1)
If @CCiProducto is NULL
  Begin
     Raiserror ('Error en campo clave CCiProducto', 1, 1)
     Return 99999
  End
Raiserror (@InfoMessage, 1, 5)


Set @InfoMessage='Obteniendo valores por omision'
Raiserror (@InfoMessage, 1, 5)
SELECT
   @CNoProducto1 =CNoProducto
 , @CCiCorto1 =CCiCorto
 , @CCiAlterno1 =CCiAlterno
 , @CCiBarra1 =CCiBarra
 , @CCiAnterior1 =CCiAnterior
 , @CCiReferencia1 =CCiReferencia
 , @CDsProducto1 =CDsProducto
 , @CCeProducto1 =CCeProducto
 , @CSNInventariable1 =CSNInventariable
 , @CSNGrabaIva1 =CSNGrabaIva
 , @CSNValorizaIva1 =CSNValorizaIva
 , @CSNCompra1 =CSNCompra
 , @CSNVenta1 =CSNVenta
 , @CSNConsumo1 =CSNConsumo
 , @NVrPrecioPublico1 =NVrPrecioPublico
 , @NVrPrecioMayorista1 =NVrPrecioMayorista
 , @NVrPrecioEspecial1 =NVrPrecioEspecial
 , @NVrCostoPromedio1 =NVrCostoPromedio
 , @CCiClasificacion11 =CCiClasificacion1
 , @CNoClasificacion11 =CNoClasificacion1
 , @CCiClasificacion21 =CCiClasificacion2
 , @CNoClasificacion21 =CNoClasificacion2
 , @CCiClasificacion31 =CCiClasificacion3
 , @CNoClasificacion31 =CNoClasificacion3
 , @CCiTipoProducto1 =CCiTipoProducto
 , @CNoTipoProducto1 =CNoTipoProducto
 , @CCiProveedor1 =CCiProveedor
 , @CNoProveedor1 =CNoProveedor
 , @DFxRegistro1 =DFxRegistro
 , @DFiIngreso1 =DFiIngreso
 , @CCiUsuarioIngreso1 =CCiUsuarioIngreso
 , @CDsEstacionIngreso1 =CDsEstacionIngreso
 , @DFmModifica1 =DFmModifica
 , @CCiUsuarioModifica1 =CCiUsuarioModifica
 , @CDsEstacionModifica1 =CDsEstacionModifica
 , @NVrUltimoPrecioPublico1 =NVrUltimoPrecioPublico
 , @NVrUltimoPrecioMayorista1 =NVrUltimoPrecioMayorista
 , @DFxUltimoPrecio1 =DFxUltimoPrecio
 , @NQnBalance1 =NQnBalance
 , @NVtBalance1 =NVtBalance
 , @CDsUnidad1 =CDsUnidad
 , @NNuInnerBox1 =NNuInnerBox
 , @NNuOuterBox1 =NNuOuterBox
 , @NQnBalanceIni1 =NQnBalanceIni
 , @NVtBalanceIni1 =NVtBalanceIni
 , @NVrCostoPromedioIni1 =NVrCostoPromedioIni
 , @NVrCostoPromedioAnt1 =NVrCostoPromedioAnt
 , @CCiPaisOrigen1 =CCiPaisOrigen
 , @CNoPaisOrigen1 =CNoPaisOrigen
 , @CCiCodigo1 =CCiCodigo
 , @CNoAuxiliar1 =CNoAuxiliar
 , @CCiCodigo21 =CCiCodigo2
 , @CNoAuxiliar21 =CNoAuxiliar2
 , @CDsPresentacion1 =CDsPresentacion
 , @CCiCodigo31 =CCiCodigo3
 , @CNoAuxiliar31 =CNoAuxiliar3
 , @NVrPrecioEspecial21 =NVrPrecioEspecial2
 , @CDsUbicacion1 =CDsUbicacion

FROM
 tblInvPRODUCTO
WHERE
 CCiProducto = @CCiProducto;

Select @ERR=@@ERROR, @RC=@@ROWCOUNT
If @ERR <> 0
  Begin
     Raiserror('Error al modificar', 1, 1)
     Return @ERR
  End
Raiserror (@InfoMessage, 1, 20)

Set @InfoMessage='Validando valores nulos'
Raiserror (@InfoMessage, 1, 20)

if @CNoProducto Is Null
   select @CNoProducto = @CNoProducto1

if @CCiCorto Is Null
   select @CCiCorto = @CCiCorto1

if @CCiAlterno Is Null
   select @CCiAlterno = @CCiAlterno1

if @CCiBarra Is Null
   select @CCiBarra = @CCiBarra1

if @CCiAnterior Is Null
   select @CCiAnterior = @CCiAnterior1

if @CCiReferencia Is Null
   select @CCiReferencia = @CCiReferencia1

if @CDsProducto Is Null
   select @CDsProducto = @CDsProducto1

if @CCeProducto Is Null
   select @CCeProducto = @CCeProducto1

if @CSNInventariable Is Null
   select @CSNInventariable = @CSNInventariable1

if @CSNGrabaIva Is Null
   select @CSNGrabaIva = @CSNGrabaIva1

if @CSNValorizaIva Is Null
   select @CSNValorizaIva = @CSNValorizaIva1

if @CSNCompra Is Null
   select @CSNCompra = @CSNCompra1

if @CSNVenta Is Null
   select @CSNVenta = @CSNVenta1

if @CSNConsumo Is Null
   select @CSNConsumo = @CSNConsumo1

if @NVrPrecioPublico Is Null
   select @NVrPrecioPublico = @NVrPrecioPublico1

if @NVrPrecioMayorista Is Null
   select @NVrPrecioMayorista = @NVrPrecioMayorista1

if @NVrPrecioEspecial Is Null
   select @NVrPrecioEspecial = @NVrPrecioEspecial1

if @NVrCostoPromedio Is Null
   select @NVrCostoPromedio = @NVrCostoPromedio1

if @CCiClasificacion1 Is Null
   select @CCiClasificacion1 = @CCiClasificacion11

if @CNoClasificacion1 Is Null
   select @CNoClasificacion1 = @CNoClasificacion11

if @CCiClasificacion2 Is Null
   select @CCiClasificacion2 = @CCiClasificacion21

if @CNoClasificacion2 Is Null
   select @CNoClasificacion2 = @CNoClasificacion21

if @CCiClasificacion3 Is Null
   select @CCiClasificacion3 = @CCiClasificacion31

if @CNoClasificacion3 Is Null
   select @CNoClasificacion3 = @CNoClasificacion31

if @CCiTipoProducto Is Null
   select @CCiTipoProducto = @CCiTipoProducto1

if @CNoTipoProducto Is Null
   select @CNoTipoProducto = @CNoTipoProducto1

if @CCiProveedor Is Null
   select @CCiProveedor = @CCiProveedor1

if @CNoProveedor Is Null
   select @CNoProveedor = @CNoProveedor1

if @DFxRegistro Is Null
   select @DFxRegistro = @DFxRegistro1

if @DFiIngreso Is Null
   select @DFiIngreso = @DFiIngreso1

if @CCiUsuarioIngreso Is Null
   select @CCiUsuarioIngreso = @CCiUsuarioIngreso1

if @CDsEstacionIngreso Is Null
   select @CDsEstacionIngreso = @CDsEstacionIngreso1

if @DFmModifica Is Null
   select @DFmModifica = @DFmModifica1

if @CCiUsuarioModifica Is Null
   select @CCiUsuarioModifica = @CCiUsuarioModifica1

if @CDsEstacionModifica Is Null
   select @CDsEstacionModifica = @CDsEstacionModifica1

if @NVrUltimoPrecioPublico Is Null
   select @NVrUltimoPrecioPublico = @NVrUltimoPrecioPublico1

if @NVrUltimoPrecioMayorista Is Null
   select @NVrUltimoPrecioMayorista = @NVrUltimoPrecioMayorista1

if @DFxUltimoPrecio Is Null
   select @DFxUltimoPrecio = @DFxUltimoPrecio1

if @NQnBalance Is Null
   select @NQnBalance = @NQnBalance1

if @NVtBalance Is Null
   select @NVtBalance = @NVtBalance1

if @CDsUnidad Is Null
   select @CDsUnidad = @CDsUnidad1

if @NNuInnerBox Is Null
   select @NNuInnerBox = @NNuInnerBox1

if @NNuOuterBox Is Null
   select @NNuOuterBox = @NNuOuterBox1

if @CCiPaisOrigen Is Null
   select @CCiPaisOrigen = @CCiPaisOrigen1

if @CNoPaisOrigen Is Null
   select @CNoPaisOrigen = @CNoPaisOrigen1

if @CCiCodigo Is Null
   select @CCiCodigo = @CCiCodigo1

if @CNoAuxiliar Is Null
   select @CNoAuxiliar = @CNoAuxiliar1

if @NQnBalanceIni Is Null
   select @NQnBalanceIni = @NQnBalanceIni1

if @NVtBalanceIni Is Null
   select @NVtBalanceIni = @NVtBalanceIni1

if @NVrCostoPromedioIni Is Null
   select @NVrCostoPromedioIni = @NVrCostoPromedioIni1

if @NVrCostoPromedioAnt Is Null
   select @NVrCostoPromedioAnt = @NVrCostoPromedioAnt1


if @CCiCodigo2 Is Null
   select @CCiCodigo2 = @CCiCodigo21

if @CNoAuxiliar2 Is Null
   select @CNoAuxiliar2 = @CNoAuxiliar21

if @CDsPresentacion Is Null
   select @CDsPresentacion = @CDsPresentacion1

if @CCiCodigo3 Is Null
   select @CCiCodigo3 = @CCiCodigo31

if @CNoAuxiliar3 Is Null
   select @CNoAuxiliar3 = @CNoAuxiliar31

if @NVrPrecioEspecial2 Is Null
   select @NVrPrecioEspecial2 = @NVrPrecioEspecial21

if @CDsUbicacion Is Null
   select @CDsUbicacion = @CDsUbicacion1

Raiserror (@InfoMessage, 1, 30)

/********************************* ZONA DE EJECUCION **********************************/
/* se ejecuta la actualización */
Set @InfoMessage='Actualizando datos'
Raiserror (@InfoMessage, 1, 30)
UPDATE
 tblInvPRODUCTO
SET
   CNoProducto =@CNoProducto
 , CCiCorto =@CCiCorto
 , CCiAlterno =@CCiAlterno
 , CCiBarra =@CCiBarra
 , CCiAnterior =@CCiAnterior
 , CCiReferencia =@CCiReferencia
 , CDsProducto =@CDsProducto
 , CCeProducto =@CCeProducto
 , CSNInventariable =@CSNInventariable
 , CSNGrabaIva =@CSNGrabaIva
 , CSNValorizaIva =@CSNValorizaIva
 , CSNCompra =@CSNCompra
 , CSNVenta =@CSNVenta
 , CSNConsumo =@CSNConsumo
 , NVrPrecioPublico =@NVrPrecioPublico
 , NVrPrecioMayorista =@NVrPrecioMayorista
 , NVrPrecioEspecial =@NVrPrecioEspecial
 , NVrCostoPromedio =@NVrCostoPromedio
 , CCiClasificacion1 =@CCiClasificacion1
 , CNoClasificacion1 =@CNoClasificacion1
 , CCiClasificacion2 =@CCiClasificacion2
 , CNoClasificacion2 =@CNoClasificacion2
 , CCiClasificacion3 =@CCiClasificacion3
 , CNoClasificacion3 =@CNoClasificacion3
 , CCiTipoProducto =@CCiTipoProducto
 , CNoTipoProducto =@CNoTipoProducto
 , CCiProveedor =@CCiProveedor
 , CNoProveedor =@CNoProveedor
 , DFxRegistro =@DFxRegistro
 , DFiIngreso =@DFiIngreso
 , CCiUsuarioIngreso =@CCiUsuarioIngreso
 , CDsEstacionIngreso =@CDsEstacionIngreso
 , DFmModifica =@DFmModifica
 , CCiUsuarioModifica =@CCiUsuarioModifica
 , CDsEstacionModifica =@CDsEstacionModifica
 , NVrUltimoPrecioPublico =@NVrUltimoPrecioPublico
 , NVrUltimoPrecioMayorista =@NVrUltimoPrecioMayorista
 , DFxUltimoPrecio =@DFxUltimoPrecio
 , NQnBalance =@NQnBalance
 , NVtBalance =@NVtBalance
 , CDsUnidad =@CDsUnidad
 , NNuInnerBox =@NNuInnerBox
 , NNuOuterBox =@NNuOuterBox
 , NQnBalanceIni =@NQnBalanceIni
 , NVtBalanceIni =@NVtBalanceIni
 , NVrCostoPromedioIni =@NVrCostoPromedioIni
 , NVrCostoPromedioAnt =@NVrCostoPromedioAnt
 , CCiPaisOrigen =@CCiPaisOrigen
 , CNoPaisOrigen =@CNoPaisOrigen
 , CCiCodigo =@CCiCodigo
 , CNoAuxiliar =@CNoAuxiliar
 , CCiCodigo2 =@CCiCodigo2
 , CNoAuxiliar2 =@CNoAuxiliar2
 , CDsPresentacion =@CDsPresentacion
 , OBjFoto =@OBjFoto
 , CCiCodigo3 =@CCiCodigo3
 , CNoAuxiliar3 =@CNoAuxiliar3
 , NVrPrecioEspecial2 =@NVrPrecioEspecial2
 , CDsUbicacion =@CDsUbicacion

WHERE
 CCiProducto = @CCiProducto;

/* verificación de error */
Select @ERRMESSAGE='Eror al modificar.';
Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
Raiserror (@InfoMessage, 1, 70)


Set @InfoMessage='Creando bodegas'
Raiserror (@InfoMessage, 1, 70)
  -- creo las bodegas y las ubicaciones para el nuevo producto
  Declare @CCiBodega	nvarchar(8)
  Declare @CCiUbicacion	nvarchar(8)
  Declare @Return_Value int

  Declare cur_Bodega INSENSITIVE CURSOR FOR
  Select
	CCiBodega
  From
	tblInvBodega
  Where
	CCeBodega=N'A'
  Order By
	CCiBodega;
  Select @ERRMESSAGE='Eror al crear Cursor de Bodegas.';
  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

  Open cur_Bodega
  Select @ERRMESSAGE='Eror al abrir Cursor de Bodegas.';
  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

  --obtengo el primer registro de bodega
  FETCH NEXT FROM cur_Bodega
	INTO @CCiBodega
  Select @ERRMESSAGE='Eror al obtener el primer registro de bodega.';
  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

  --ejecuto este bloque mientras el fetch sea diferente de -1
  --es -1 cuando no hay datos en el cursor o se alcanza el fin del cursor
  WHILE @@FETCH_STATUS <> -1
	BEGIN
	  Declare cur_Ubicacion INSENSITIVE CURSOR FOR
	  Select
		CCiUbicacion
	  From
		tblInvUbicacion
	  Where
		CCiBodega=@CCiBodega
		And CCeUbicacion=N'A'
	  Order By
		CCiUbicacion;
	  Select @ERRMESSAGE='Eror al crear Cursor de Ubicaciones.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

	  Open cur_Ubicacion
	  Select @ERRMESSAGE='Eror al abrir Cursor de Ubicaciones.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

	  --obtengo el primer registro de ubicacion
	  FETCH NEXT FROM cur_Ubicacion
		INTO @CCiUbicacion
	  Select @ERRMESSAGE='Eror al obtener el primer registro de ubicacion.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

	  --ejecuto este bloque mientras el fetch sea diferente de -1
	  --es -1 cuando no hay datos en el cursor o se alcanza el fin del cursor
	  WHILE @@FETCH_STATUS <> -1
		BEGIN
			-- inserto las bodegas y ubicaciones en la tabla de saldos
			IF not exists (Select * From tblInvSALDOBODEGA Where CCiProducto=@CCiProducto And CCiBodega=@CCiBodega And CCiUbicacion=@CCiUbicacion)
			  BEGIN
				EXEC @Return_Value=spiInvSaldoBodega
					 @CCiProducto, @CCiBodega, @CCiUbicacion, @CDsUnidad
					 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0
				If @Return_Value <> 0
				  BEGIN
					  Select @ERRMESSAGE='Error al insertar las bodega y ubicaciones.';
					  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
				  END
			  END

		  --obtengo el primer siguiente de ubicacion
		  FETCH NEXT FROM cur_Ubicacion
			INTO @CCiUbicacion
		  Select @ERRMESSAGE='Eror al obtener el siguiente registro de ubicacion.';
		  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
		END

		CLOSE cur_Ubicacion
		DEALLOCATE cur_Ubicacion

	  --obtengo el siguiente registro de bodega
	  FETCH NEXT FROM cur_Bodega
		INTO @CCiBodega
	  Select @ERRMESSAGE='Eror al obtener el siguiente registro de bodega.';
	  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
	END

  CLOSE cur_Bodega
  DEALLOCATE cur_Bodega

Raiserror (@InfoMessage, 1, 100)

Raiserror ('Listo...', 1, 1)

Return 0

ERR_HANDLER:
   RAISERROR(@ERRMESSAGE,11,1)
   RETURN @ERR
GO
