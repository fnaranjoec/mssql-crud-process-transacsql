SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Objeto:  StoredProcedure [dbo].[spdInvProducto]    Fecha de la secuencia de comandos: 03/15/2010 16:10:07 ******/
CREATE PROCEDURE [dbo].[spdInvProducto]
 @CCiProducto nvarchar(20)=null
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 ---- para uso del procedimiento
 --Declare @NUMPEDIDO int

 -- para control de errores --
 Declare @ERR		int
 Declare @RC		int
 Declare @ROWS		int
 SET @ROWS=0

/******************  ZONA DE ASERCION *********************/
 /* Validación de campos clave */
 Set @InfoMessage='Validando Clave Principal'
 Raiserror (@InfoMessage, 1, 1)
 If @CCiProducto is NULL
   Begin
     Raiserror ('Error en campo clave CCiProducto', 1, 1)
     Return 99999
   End
 Raiserror (@InfoMessage, 1, 5)

/******************  ZONA DE EJECUCION *******************/

-- OJO Aqui crear procedimiento para verificar si hay saldo o movimientos kardex
If (Select NQnBalance From tblInvProducto Where CCiProducto=@CCiProducto)<>0
   Begin
     Raiserror ('No se puede Eliminar, el producto aun tiene saldo.<MSG>', 1, 1)
     Return 99999
   End

If Exists(Select CCiProducto From tblInvKardex Where CCiProducto=@CCiProducto)
   Begin
     Raiserror ('No se puede Eliminar, el producto tiene movimientos.<MSG>', 1, 1)
     Return 99999
   End
-- en el producto, solo se podra borrar si esta en cero completamente

/* ejecución del insert */
 Set @InfoMessage='Eliminando Producto'
 Raiserror (@InfoMessage, 1, 5)
 DELETE tblInvPRODUCTO
 WHERE CCiProducto=@CCiProducto
 /* verificación de error */
 Select @ERR=@@ERROR, @RC=@@ROWCOUNT
 If @ERR <> 0
   Begin
      Raiserror('Error al borrar', 1, 1)
      Return @ERR
   End

 Raiserror (@InfoMessage, 1, 100)

 Raiserror ('Listo', 1, 5)

return 0
GO
