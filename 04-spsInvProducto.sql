SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Objeto:  StoredProcedure [dbo].[spsInvProducto]    Fecha de la secuencia de comandos: 03/15/2010 16:10:07 ******/
CREATE PROCEDURE [dbo].[spsInvProducto]
	@CCiProducto	nvarchar(20)
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 ---- para uso del procedimiento
 --Declare @NUMPEDIDO int

 -- para control de errores --
 Declare @ERR		int
 Declare @RC		int
 Declare @ROWS		int
 SET @ROWS=0

/******************  ZONA DE ASERCION *********************/
 /* Validación de campos clave */


/******************  ZONA DE EJECUCION *******************/
/* ejecución del insert */
	SELECT CCiProducto
      ,CNoProducto
      ,CCiCorto
      ,CCiAlterno
      ,CCiBarra
      ,CCiAnterior
      ,CCiReferencia
      ,CDsProducto
      ,CCeProducto
      ,CSNInventariable
      ,CSNGrabaIva
      ,CSNValorizaIva
      ,CSNCompra
      ,CSNVenta
      ,CSNConsumo
      ,NVrPrecioPublico
      ,NVrPrecioMayorista
      ,NVrPrecioEspecial
      ,NvrCostoPromedio
      ,CCiClasificacion1
      ,CNoClasificacion1
      ,CCiClasificacion2
      ,CNoClasificacion2
      ,CCiClasificacion3
      ,CNoClasificacion3
      ,CCiTipoProducto
      ,CNoTipoProducto
      ,CCiProveedor
      ,CNoProveedor
      ,DFxRegistro
      ,CCiUsuarioIngreso
      ,CDsEstacionIngreso
      ,DFiIngreso
      ,CCiUsuarioModifica
      ,CDsEstacionModifica
      ,DFmModifica
      ,NVrUltimoPrecioPublico
      ,NVrUltimoPrecioMayorista
      ,DFxUltimoPrecio
      ,NQnBalance
      ,NVtBalance
      ,CDsUnidad
      ,NNuInnerBox
      ,NNuOuterBox
      ,NQnBalanceIni
      ,NVtBalanceIni
      ,NvrCostoPromedioIni
      ,NVrCostoPromedioAnt
      ,CCiPaisOrigen
      ,CNoPaisOrigen
      ,CCiCodigo
      ,CNoAuxiliar
      ,CCiCodigo2
      ,CNoAuxiliar2
      ,CDsPresentacion
      ,OBjFoto
      ,CCiCodigo3
      ,CNoAuxiliar3
      ,NVrPrecioEspecial2
      ,CDsUbicacion
FROM [dbo].[tblInvPRODUCTO]
Where CCiProducto=@CCiProducto
Order By CCiProducto ;

/* verificación de error */
Select @ERR=@@ERROR, @RC=@@ROWCOUNT
if @ERR <> 0
begin
   raiserror('Error al consultar', 1, 1)
   return @ERR
end

return 0
GO
