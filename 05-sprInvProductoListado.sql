SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO

/****** Objeto:  StoredProcedure [dbo].[spiIvProducto]    Fecha de la secuencia de comandos: 03/15/2010 16:10:07 ******/
CREATE PROCEDURE [dbo].[sprInvProductoListado]
  @CSNConCeroStock		nchar(1)='N'
  , @CNuClasificacion	nchar(1)='1'
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 ---- para uso del procedimiento
 --Declare @NUMPEDIDO int

 -- para control de errores --
 Declare @ERR		int
 Declare @RC		int
 Declare @ROWS		int
 SET @ROWS=0

/******************  ZONA DE ASERCION *********************/
 /* Validación de campos clave */


/******************  ZONA DE EJECUCION *******************/
/* ejecución del insert */
Declare @Query		nvarchar(max)

Set @Query=""
Set @Query=@Query + "Select " + char(13)
Set @Query=@Query + "  PRO.CCiProducto " + char(13)
Set @Query=@Query + "  , PRO.CNoProducto " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioPublico " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioMayorista " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioEspecial " + char(13)

If @CNuClasificacion='1'
	Set @Query=@Query + "  , IsNull(CLA1.CNoClasificacion1,'<Sin Clasificaion 1>') As CNoClasificacion " + char(13);

If @CNuClasificacion='2'
	Set @Query=@Query + "  , IsNull(CLA2.CNoClasificacion2,'<Sin Clasificacion 2>') As CNoClasificacion " + char(13);

If @CNuClasificacion='3'
	Set @Query=@Query + "  , IsNull(CLA3.CNoClasificacion3,'<Sin Clasificacion 3>') As CNoClasificacion " + char(13);

Set @Query=@Query + "  , IsNull(Sum(SAL.NQnExistencia),0) As NQnExistencia" + char(13)
Set @Query=@Query + "From " + char(13)
Set @Query=@Query + "  tblInvProducto PRO " + char(13)
Set @Query=@Query + "    Inner Join tblInvSaldoBodega SAL " + char(13)
Set @Query=@Query + "      ON SAL.CCiProducto=PRO.CCiProducto " + char(13)
Set @Query=@Query + "    Inner Join tblInvBodega BOD " + char(13)
Set @Query=@Query + "      ON BOD.CCiBodega=SAL.CCiBodega And BOD.CCiTipoBodega='01' " + char(13)

If @CNuClasificacion='1'
	Begin
	  Set @Query=@Query + "  Left Outer Join tblInvClasif1Producto CLA1 " + char(13)
	  Set @Query=@Query + "    ON CLA1.CCiClasificacion1=PRO.CCiClasificacion1 " + char(13)
	End

If @CNuClasificacion='2'
	Begin
	  Set @Query=@Query + "  Left Outer Join tblInvClasif2Producto CLA2 " + char(13)
	  Set @Query=@Query + "    ON CLA2.CCiClasificacion1=PRO.CCiClasificacion1 " + char(13)
	  Set @Query=@Query + "    AND CLA2.CCiClasificacion2=PRO.CCiClasificacion2 " + char(13)
	End

If @CNuClasificacion='3'
	Begin
	  Set @Query=@Query + "  Left Outer Join tblInvClasif3Producto CLA3 " + char(13)
	  Set @Query=@Query + "    ON CLA3.CCiClasificacion1=PRO.CCiClasificacion1 " + char(13)
	  Set @Query=@Query + "    AND CLA3.CCiClasificacion2=PRO.CCiClasificacion2 " + char(13)
	  Set @Query=@Query + "    AND CLA3.CCiClasificacion3=PRO.CCiClasificacion3 " + char(13)
	End

Set @Query=@Query + "Where " + char(13)
Set @Query=@Query + "  PRO.CCeProducto='A' " + char(13)
--Set @Query=@Query + "  And BOD.CCiTipoBodega='01' " + char(13)
Set @Query=@Query + "Group By " + char(13)
Set @Query=@Query + "  PRO.CCiProducto " + char(13)
Set @Query=@Query + "  , PRO.CNoProducto " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioPublico " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioMayorista " + char(13)
Set @Query=@Query + "  , PRO.NVrPrecioEspecial " + char(13)

If @CNuClasificacion='1'
	Set @Query=@Query + "  , CLA1.CNoClasificacion1 " + char(13);

If @CNuClasificacion='2'
	Set @Query=@Query + "  , CLA2.CNoClasificacion2 " + char(13);

If @CNuClasificacion='3'
	Set @Query=@Query + "  , CLA3.CNoClasificacion3 " + char(13);

If @CSNConCeroStock = 'N'
	Begin
	  Set @Query=@Query + "Having " + char(13)
	  Set @Query=@Query + "  IsNull(Sum(SAL.NQnExistencia),0) <> 0 " + char(13)
	End


-- ejecuto comando
Execute sp_executesql @Query;
Set @Query=""


/* verificación de error */
Select @ERR=@@ERROR, @RC=@@ROWCOUNT
if @ERR <> 0
begin
   raiserror('Error al consultar', 1, 1)
   return @ERR
end

return 0
GO
