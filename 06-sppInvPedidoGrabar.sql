SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fredy Naranjo P. veritasoft@gmail.com
-- Create date: 01-01-2010
-- Description:	Genera los procesos de facturacion
-- @prmCNuPedido, numero de factura que se grabo en la tabla de facturas tblFacFacturaCabecera
-- =============================================
CREATE PROCEDURE [dbo].[sppInvPedidoGrabar]
  @CCiCia			nchar(3)=null
, @CCiDivision		nchar(3)=null
, @CCiSucursal		nchar(3)=null
, @prmCNuPedido		nchar(18)=Null
, @CSNGenerarSec	nchar(1)=null
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 -- para control de errores --
 Declare @ERR		 int
 Declare @RC		 int
 Declare @ROWS		 int
 Declare @ERRTYPE	 nvarchar(20)
 Declare @ERRMESSAGE nvarchar(max)
 SET @ROWS=0


-- Declaro variables para cada registro del detalle del pedio
-- que voy a procesar
Declare @CNuFactura		nchar(18)
Declare @CNuPedido		nchar(18)
Declare @CCiProducto	nvarchar(20)
Declare @CCiBodega		nvarchar(8)
Declare @CCiUbicacion	nvarchar(8)
Declare @NQnExistencia	decimal(16,2)
Declare @NQnExistenciaPEDIDOS	decimal(16,2)
Declare @NQnDespachado	decimal(16,2)
Declare @NQnFacturado	decimal(16,2)
Declare @NVrPrecio		decimal(16,2)
Declare @NVrCosto		decimal(16,2)
Declare @NVtTotalCosto	decimal(16,2)
Declare @Fetch_Detalle	int

Declare @CCiBodegaPEDIDOS		nvarchar(8)
Declare @CCiUbicacionPEDIDOS	nvarchar(8)

--para secuencia de pedido
Declare @NNuSec					numeric(18, 0)
Declare @CNuPedidoORIGINAL		nchar(18)
Declare @CNuPedidoNUEVO			nchar(18)
Declare @CCiCodCiaDivSuc		nchar(9)

Set @CNuPedidoORIGINAL=N'0'
Set @CNuPedidoNUEVO=N'0'

-- Variable para capturar la respuesta de un procedimiento externo
DECLARE @ReturnValue INT

/********************** ZONA DE ASERCION *********************************/
 /* Validación de campos clave */
-- Set @InfoMessage='Validando Clave Principal'
-- Raiserror (@InfoMessage, 1, 1)

 if @CCiCia is NULL
 begin
   raiserror ('Error en campo clave CCiCia', 1, 1)
   return 99999
 end

 if @CCiDivision is NULL
 begin
   raiserror ('Error en campo clave CCiDivision', 1, 1)
   return 99999
 end

 if @CCiSucursal is NULL
 begin
   raiserror ('Error en campo clave CCiSucursal', 1, 1)
   return 99999
 end

If @prmCNuPedido is NULL
   Begin
     Raiserror ('Error en campo clave CNuPedido.',11,1)
     Return 99999
   End

If @CSNGenerarSec is NULL
   Begin
     Raiserror ('Error en campo clave CSNGenerarSec.',11,1)
     Return 99999
   End

 Raiserror (@InfoMessage, 1, 15)

 -- ////////////// OBTENER SECUENCIA DEL PEDIDO ////////////////
-- Set @InfoMessage='Obteniendo secuencia de pedido y reemplazando el numero temporal'
-- Raiserror (@InfoMessage, 1, 15)
 Set @CCiCodCiaDivSuc=rtrim(ltrim(@CCiCia)) + rtrim(ltrim(@CCiDivision)) + rtrim(ltrim(@CCiSucursal));
 Set @CNuPedidoORIGINAL=@prmCNuPedido;

--// Obtengo parametros
DECLARE	@CTxTexto				nvarchar(100)
Declare @CCiTipoSec				nchar(4)
Declare @CCiPedidosSec			nvarchar(10)
Declare @CCiTipoCompra			nchar(3)

EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = @CCiCia,
		@CCiDivision = @CCiDivision,
		@CCiSucursal = @CCiSucursal,
		@CCiParametro = N'SECINVTP',
		@CTxTexto = @CTxTexto OUTPUT ;
Set @CCiTipoSec=@CTxTexto ;

EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = @CCiCia,
		@CCiDivision = @CCiDivision,
		@CCiSucursal = @CCiSucursal,
		@CCiParametro = N'SECINVPD',
		@CTxTexto = @CTxTexto OUTPUT ;
Set @CCiPedidosSec=@CTxTexto ;

--...
 --// Verifico si hay que generar secuencia o no
 IF @CSNGenerarSec=N'S'
   Begin

    --Obteniendo tipo de pedido local o exterior
	Select @CCiTipoCompra=CCiTipoCompra
	From tblInvPedidoCabecera
	Where CNuPedido=@prmCNuPedido;

	 EXEC	@ReturnValue = [dbo].[sppGenConsecutivoGeneral]
			@Consecutivo = @CCiTipoSec,
			@CCiCiaDivSuc = @CCiCodCiaDivSuc,
			@CCiTipoConsec = @CCiPedidosSec,
			@BRollBack = 0,
			@BSoloConsulta = 0,
			@NNuSec = @NNuSec OUTPUT;

	 If @ReturnValue=-1
	   Begin
		Select @ERRTYPE='SECUENCE';
		GOTO ERR_HANDLER;
	   End

	 Set @CNuPedidoNUEVO=Cast(@NNuSec As nchar(18));

	 --// Actualizo al nuevo secuencial
	 UPDATE tblInvPedidoCabecera
	 Set CNuPedido=@CNuPedidoNUEVO
	 Where CCiCia=@CCiCia
	   And CCiDivision=@CCiDivision
	   And CCiSucursal=@CCiSucursal
	   And CNuPedido=@CNuPedidoORIGINAL ;
	 Select @ERRTYPE='SECUENCEUPDATE';
	 Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

   End
ELSE
   Set @CNuPedidoNUEVO=@CNuPedidoORIGINAL ;

 --// Esto se puso para GRUPO AR, para aprobar apenas se graba
 EXEC @ReturnValue = sppInvPedidoAprobar @CCiCia, @CCiDivision, @CCiSucursal, @CNuPedidoNUEVO;
 IF @ReturnValue<0
	BEGIN
	 Select @ERRTYPE='APROBAR'
	 GOTO ERR_HANDLER
	END
 --//...


 Raiserror (@InfoMessage, 1, 100)

 Raiserror ('Listo...', 1, 1)

 RETURN 0

ERR_HANDLER:

   SELECT @ERRMESSAGE=
   CASE
	 WHEN @ERRTYPE='CREATE' THEN 'Error al crear cursor.'
	 WHEN @ERRTYPE='OPEN' THEN 'Error al abrir cursor.'
	 WHEN @ERRTYPE='STATUS' THEN 'Error al cambiar status del pedido.'
	 WHEN @ERRTYPE='EXISTENCIA' THEN 'No se obtener la existencia del item actual.'
	 WHEN @ERRTYPE='NEXT' THEN 'No se pudo procesar el siguiente registro.'
	 WHEN @ERRTYPE='FACTURADO' THEN 'No se pudo acumular en el pedido lo facturado.'
	 WHEN @ERRTYPE='REVERSE' THEN 'Error al reversar el numero de factura.'
	 WHEN @ERRTYPE='SECUENCE' THEN 'Error al obtener el numero de factura.'
	 WHEN @ERRTYPE='SECUENCEUPDATE' THEN 'Error al poner el numero de factura.'
	 WHEN @ERRTYPE='APROBAR' THEN 'Error al poner tratar de aprobar pedido.'
   END

   -- Reversar el numero de la factura al numero aleatorio
   UPDATE tblInvPedidoCabecera
   Set CNuPedido=@CNuPedidoORIGINAL
   Where CCiCia=@CCiCia
     And CCiDivision=@CCiDivision
     And CCiSucursal=@CCiSucursal
     And CNuPedido=@CNuPedidoNUEVO ;
   --Select @ERRTYPE='REVERSE'
   --Select @ERR=@@ERROR, @RC=@@ROWCHEN @ERRTYPE='EXISTENCIA' THEN 'No se obtener la existencia del item actual.'
   RAISERROR(@ERRMESSAGE,11,1)
   RETURN @ERR
GO
