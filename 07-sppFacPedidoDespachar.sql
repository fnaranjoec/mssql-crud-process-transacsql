SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fredy Naranjo Petroche
-- Create date: 01-01-2010
-- Description:	Despacha el pedido
-- =============================================
CREATE PROCEDURE [dbo].[sppFacPedidoDespachar]
    @prmCNuPedido		nchar(18)=Null
  , @prmNNuSecuencia	int=null
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 Declare @CCePedido	  nchar(1)
 Declare @BSNSeparado bit

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 -- para control de errores --
 Declare @ERR		 int
 Declare @RC		 int
 Declare @ROWS		 int
 Declare @ERRTYPE	 nvarchar(20)
 Declare @ERRMESSAGE nvarchar(max)
 SET @ROWS=0

/*
A=Activo
X=Anulado
P=Aprobado
N=Negado
D=Despachado
I=Inactivo o Cerrado
*/


 /********************** ZONA DE ASERCION *********************************/
 /* Validación de campos clave */
 Set @InfoMessage='Validando Clave Principal'
 Raiserror (@InfoMessage, 1, 1)

 If @prmCNuPedido is NULL
   Begin
     Raiserror ('Error en campo clave CNuPedido.',11,1)
     Return 99999
   End

 If @prmNNuSecuencia is NULL
   Begin
     Raiserror ('Error en campo clave NNuSecuencia.',11,1)
     Return 99999
   End

/********************** ZONA DE PARAMETROS *********************************/

DECLARE	@CTxTexto							nvarchar(100)
DECLARE	@NNuValor							decimal(16,4)
Declare @CCiModulo							nchar(3)
Declare @tblEstados							table(CTxStatus nchar(1), CDsStatus nvarchar(50));
Declare @CTxStatusAprobado					nchar(1)
Declare @CTxStatusDespachado				nchar(1)
Declare @CTxStatusCartera					nchar(1)
Declare @CTxStatusParcial					nchar(1)
Declare @CTxStatusTotal						nchar(1)
Declare @CTxStatusPedidoContabilizado		nchar(1)
Declare @CTxStatusContabilizado				nchar(1)
Declare @CCiConceptoIngresaPedido			nchar(8)
Declare @CCiTipoIngreso						nchar(1)
Declare @CCiTipoEgreso						nchar(1)
Declare @CCiTipoSec							nvarchar(10)
Declare @CCiFacturasSec						nvarchar(10)
Declare @CCiTipoDocFactura					nvarchar(10)
Declare @CCiPedidoDespacho					nvarchar(10)

Declare @CCiBodegaPEDIDOS					nvarchar(8)
Declare @CCiUbicacionPEDIDOS				nvarchar(8)
Declare @CCiDivBodega						nchar(3)

--// Modulo Facturacion
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'MODFAC',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno MODFAC', 1, 1)  return 99999 end
Set @CCiModulo=@CTxTexto;

--// Bodega de Pedidos Clientes
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CDFACBDCL',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDFACBDCL', 1, 1)  return 99999 end
Set @CCiBodegaPEDIDOS=@CTxTexto ;

--// Ubicacion de Pedidos Clientes
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CDFACUBCL',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDFACUBCL', 1, 1)  return 99999 end
Set @CCiUbicacionPEDIDOS=@CTxTexto;

--// Tipo Ingreso Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPINVMVIN',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPINVMVIN', 1, 1)  return 99999 end
Set @CCiTipoIngreso=@CTxTexto;

--// Tipo Egreso de Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPINVMVEG',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPINVMVEG', 1, 1)  return 99999 end
Set @CCiTipoEgreso=@CTxTexto;

--// Concepto Ingreso Pedido Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CCFACPDIN',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CCFACPDIN', 1, 1)  return 99999 end
Set @CCiConceptoIngresaPedido=@CTxTexto;

--// Tipo Documento Despacho Facturacion
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPFACDS',
		@CTxTexto = @CTxTexto OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPFACDS', 1, 1)  return 99999 end
Set @CCiPedidoDespacho=@CTxTexto;

--// Codigo de Division Default para bodega
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CDINVDIV',
		@CTxTexto = @CTxTexto OUTPUT ,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDINVDIV', 1, 1)  return 99999 end
Set @CCiDivBodega=@CTxTexto;

--//Cargo tabla temporal de estados de documento
Insert Into @tblEstados EXEC [dbo].[spsFacPedidoStatusLista] @CCiCia = N'', @CCiDivision = N'', @CCiSucursal = N'', @CCiAplicacion = N'SISCO' ;
--//Estado Aprobado
Select @CTxStatusAprobado=CTxStatus From @tblEstados WHERE CDsStatus Like N'APRO%';
--//Estado Despachado
Select @CTxStatusDespachado=CTxStatus From @tblEstados WHERE CDsStatus Like N'DESP%';
--//Estado Recepcion Parcial
Select @CTxStatusParcial=CTxStatus From @tblEstados WHERE CDsStatus Like N'PARC%';
--//Estado Recepcion Total
Select @CTxStatusTotal=CTxStatus From @tblEstados WHERE CDsStatus Like N'TOTA%';
--//Estado Pedido Contabilizado
Select @CTxStatusPedidoContabilizado=CTxStatus From @tblEstados WHERE CDsStatus Like N'CONTA%';

/********************** ZONA DE EJECUCION *********************************/

 --Select
	--@CCePedido=CCePedido, @BSNSeparado=BSNSeparado
 --From
	--tblFacPedidoCabecera
 --Where
	--CNuPedido=@prmCNuPedido;

 --If Not @CCePedido In (@CTxStatusAprobado, @CTxStatusDespachado)
 --   Begin
	--   Raiserror ('El pedido no se puede procesar, no ha sido aprobado o despachado.',11,1)
	--   Return 99999
	--End
 --Raiserror (@InfoMessage, 1, 5)

/*** AQUI VA PROCEDIMIENTO PARA DESPACHAR EL PEDIDO **/

/*************************************************************************/
/*       Separo el pedido original en pedidos parciales por compania     */
/*************************************************************************/


-- Declaro variables para cada registro del detalle del pedio
-- que voy a procesar
Declare @CCiCia					nchar(3)
Declare @CCiDivision			nchar(3)
Declare @CCiSucursal			nchar(3)
Declare @CNuPedido				nchar(18)
Declare @CCiProducto			nvarchar(20)
Declare @NNuLinea				int
Declare @CCiBodega				nvarchar(8)
Declare @CCiUbicacion			nvarchar(8)
Declare @NQnDespachado			decimal(16,2)
Declare @NVrPrecio				decimal(16,2)
Declare @NVrCosto				decimal(16,2)
Declare @NVtTotalCosto			decimal(16,2)
Declare @Fetch_Detalle			int
Declare @NQnExistencia			decimal(16,2)
Declare @NNuLineaGenerada		int
--//***OJO*** No borrar esto solo se quito para grupo AR
---- AQUI DEBO LEER TABLA DE PARAMETROS PARA CARGAR LA BODEGA y UBICACION de PEDIDOS
--If @BSNSeparado=0
--  Begin
--	Select @CCiBodegaPEDIDOS='0009', @CCiUbicacionPEDIDOS='01'; --manda la mercaderia a bodega de pedidos
--  End
--Else
--  Begin
--	Select @CCiBodegaPEDIDOS='0004', @CCiUbicacionPEDIDOS='01'; --manda la mercaderia a bodega de separados
--  End

--Declaro Cursor
Set @InfoMessage='Abriendo detalle de pedido'
Raiserror (@InfoMessage, 1, 5)


--// Aqui @CCiDivBodega reemplaza a la division de ventas que esta grabada en el despacho
Set @NNuLineaGenerada=0;
DECLARE cur_PedidoDetalle CURSOR FOR
	SELECT CCiCia, @CCiDivBodega, CCiSucursal
		, CNuPedido, CCiProducto, NNuLinea, NQnDespachado
		, NVrPrecio, NVrCosto, NVtTotalCosto
		, CCiBodega, CCiUbicacion
	FROM tblFacDespachoDetalle
	WHERE CNuPedido=@prmCNuPedido And NNuSecuencia=@prmNNuSecuencia
	ORDER BY CCiCia, CCiDivision, CCiSucursal, CNuPedido, CCiProducto, NNuLinea ;
	Select @ERRTYPE='CREATE';
	Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

--Abro el cursor
OPEN cur_PedidoDetalle;
Raiserror (@InfoMessage, 1, 15);

--Leo el primer registro y obtengo el status del fetch
Set @InfoMessage='Procesando detalle'
Raiserror (@InfoMessage, 1, 15)
FETCH NEXT FROM cur_PedidoDetalle
	INTO @CCiCia, @CCiDivision, @CCiSucursal, @CNuPedido, @CCiProducto, @NNuLinea, @NQnDespachado
	   , @NVrPrecio, @NVrCosto, @NVtTotalCosto
	   , @CCiBodega, @CCiUbicacion;
	Select @ERRTYPE='OPEN';
	Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

--el fetch se carga cada vez que paso a otro registro
SET @Fetch_Detalle=@@FETCH_STATUS

--ejecuto este bloque mientras el fetch sea diferente de -1
--es -1 cuando no hay datos en el cursor o se alcanza el fin del cursor
WHILE @Fetch_Detalle <> -1
	BEGIN
		Set @NNuLineaGenerada=@NNuLineaGenerada+1;

		Select @NQnExistencia=NQnExistencia
		From tblInvSaldoBodega
		Where CCiCia = @CCiCia
		  And CCiDivision = @CCiDivision
		  And CCiSucursal = @CCiSucursal
		  And CCiBodega=@CCiBodega
		  And CCiUbicacion=@CCiUbicacion
		  And CCiProducto=@CCiProducto;

		-- ///////////////// DESCARGO de la bodega normal //////////////////
		IF @NQnDespachado<=@NQnExistencia  -- *** hay existencia completa ***
			BEGIN
				-- actualizo PRODUCTO
				UPDATE tblInvPRODUCTO
				SET NQnBalance=IsNull(NQnBalance,0)-@NQnDespachado
				  , NVtBalance=IsNull(NVtBalance,0)-(@NQnDespachado*@NVrCosto)
				WHERE CCiProducto=@CCiProducto;
				Select @ERRTYPE='DESCARGO'
				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

				--actualizo BODEGA
				UPDATE tblInvSaldoBodega
				SET NQnExistencia=IsNull(NQnExistencia,0)-@NQnDespachado
				  , NQnBalance=IsNull(NQnBalance,0)-@NQnDespachado
				  , NVtBalance=IsNull(NVtBalance,0)-(@NQnDespachado*@NVrCosto)
				WHERE CCiCia = @CCiCia
				  And CCiDivision = @CCiDivision
				  And CCiSucursal = @CCiSucursal
				  And CCiBodega=@CCiBodega
				  And CCiUbicacion=@CCiUbicacion
				  And CCiProducto=@CCiProducto;
				Select @ERRTYPE='DESCARGO'
				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER
			END
		ELSE -- *** no hay stock suficiente ***
			BEGIN

				----ESTO LO DESHABILITE PARA QUIMISER PARA QUE DESCARGUE LO QUE SE PIDIO REALMENTE ASI GENERE (-)
				---- despacho lo que hay en stock
				--SELECT @NQnDespachado=@NQnExistencia;

				-- actualizo PRODUCTO
				UPDATE tblInvPRODUCTO
				SET NQnBalance=IsNull(NQnBalance,0)-@NQnDespachado
				  , NVtBalance=IsNull(NVtBalance,0)-(@NQnDespachado*@NVrCosto)
				WHERE CCiProducto=@CCiProducto;
				Select @ERRTYPE='DESCARGO'
				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

				-- actualizo BODEGA
				UPDATE tblInvSaldoBodega
				SET NQnExistencia=IsNull(NQnExistencia,0)-@NQnDespachado
				  , NQnBalance=IsNull(NQnBalance,0)-@NQnDespachado
				  , NVtBalance=IsNull(NVtBalance,0)-(@NQnDespachado*@NVrCosto)
				WHERE CCiCia = @CCiCia
				  And CCiDivision = @CCiDivision
				  And CCiSucursal = @CCiSucursal
				  And CCiBodega=@CCiBodega
				  And CCiUbicacion=@CCiUbicacion
				  And CCiProducto=@CCiProducto;
				Select @ERRTYPE='DESCARGO'
				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER
			END

		/** AQUI VA LA PARTE PARA INSERTAR EL MOVIMIENTO EN EL KARDEX: Descargo bodega normal **/
		INSERT INTO tblInvKardex
		(
			  NNuLinea
			, CCiProducto
			, CCiBodegaOrigen
			, CCiUbicacionOrigen
			, CCiBodegaDestino
			, CCiUbicacionDestino
			, CCiTipoMov
			, CCiConcepto
			, CNuMovimiento
			, CNuReferencia
			, DFxRegistro
			, DFiIngreso
			, CCiUsuarioIngreso
			, CDsEstacionIngreso
			, DFmModifica
			, CCiUsuarioModifica
			, CDsEstacionModifica
			, NQnCantidad
			, NVrPrecio
			, NVrCosto
			, NVrNuevoCosto
			, NQnBalanceGen
			, NVtBalanceGen
			, NQnBalanceParc
			, NVtBalanceParc
			, CCiCia
			, CCiDivision
			, CCiSucursal
			, CCiCia2
			, CCiDivision2
			, CCiSucursal2
		)
		Select
			  @NNuLineaGenerada
			, @CCiProducto
			, @CCiBodega
			, @CCiUbicacion
			, @CCiBodegaPEDIDOS
			, @CCiUbicacionPEDIDOS
			, @CCiTipoEgreso				-- CCiTipo
			, @CCiPedidoDespacho			-- CCiMovimiento
			, @CNuPedido					-- CNuMovimiento
			, (Select CNuReferencia From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CNuReferencia
			, getdate()
			, getdate() --(Select DFiIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	--getdate()
			, (Select CCiUsuarioIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CCiUsuarioIngreso
			, (Select CDsEstacionIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CDsEstacionIngreso
			, null
			, null
			, null
			, @NQnDespachado*(-1)
			, @NVrPrecio
			, @NVrCosto
			, @NVrCosto
			, (Select IsNull(NQnBalance,0) From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select IsNull(NVtBalance,0) From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select IsNull(NQnBalance,0) From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion And CCiProducto=@CCiProducto)
			, (Select IsNull(NVtBalance,0) From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion And CCiProducto=@CCiProducto)
			, @CCiCia, @CCiDivision, @CCiSucursal
			, @CCiCia, @CCiDivision, @CCiSucursal;
		Select @ERRTYPE='KARDEX DESCARGO NORMAL';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;


		-- ////////////////// cargo la bodega pedidos //////////////////////
		-- actualizo PRODUCTO
		UPDATE tblInvPRODUCTO
		SET NQnBalance=IsNull(NQnBalance,0)+@NQnDespachado
		  , NVtBalance=IsNull(NVtBalance,0)+(@NQnDespachado*@NVrCosto)
		WHERE CCiProducto=@CCiProducto;
		Select @ERRTYPE='CARGO'
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

		-- actualizo BODEGA
		UPDATE tblInvSaldoBodega
		SET NQnExistencia=IsNull(NQnExistencia,0)+@NQnDespachado
		  , NQnBalance=IsNull(NQnBalance,0)+@NQnDespachado
		  , NVtBalance=IsNull(NVtBalance,0)+(@NQnDespachado*@NVrCosto)
		WHERE CCiCia = @CCiCia
		  And CCiDivision = @CCiDivision
		  And CCiSucursal = @CCiSucursal
		  And CCiBodega=@CCiBodegaPEDIDOS
		  And CCiUbicacion=@CCiUbicacionPEDIDOS
		  And CCiProducto=@CCiProducto;
		Select @ERRTYPE='CARGO'
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

		/** AQUI VA LA PARTE PARA INSERTAR EL MOVIMIENTO EN EL KARDEX: Cargo bodega de despachos **/
		INSERT INTO tblInvKardex
		(
			  NNuLinea
			, CCiProducto
			, CCiBodegaOrigen
			, CCiUbicacionOrigen
			, CCiBodegaDestino
			, CCiUbicacionDestino
			, CCiTipoMov
			, CCiConcepto
			, CNuMovimiento
			, CNuReferencia
			, DFxRegistro
			, DFiIngreso
			, CCiUsuarioIngreso
			, CDsEstacionIngreso
			, DFmModifica
			, CCiUsuarioModifica
			, CDsEstacionModifica
			, NQnCantidad
			, NVrPrecio
			, NVrCosto
			, NVrNuevoCosto
			, NQnBalanceGen
			, NVtBalanceGen
			, NQnBalanceParc
			, NVtBalanceParc
			, CCiCia
			, CCiDivision
			, CCiSucursal
			, CCiCia2
			, CCiDivision2
			, CCiSucursal2
		)
		Select
			  @NNuLineaGenerada
			, @CCiProducto
			, @CCiBodegaPEDIDOS
			, @CCiUbicacionPEDIDOS
			, @CCiBodega
			, @CCiUbicacion
			, @CCiTipoIngreso		-- CCiTipo
			, @CCiPedidoDespacho	-- CCiMovimiento
			, @CNuPedido			-- CNuMovimiento
			, (Select CNuReferencia From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CNuReferencia
			, getdate()
			, getdate() --(Select DFiIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	--getdate()
			, (Select CCiUsuarioIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CCiUsuarioIngreso
			, (Select CDsEstacionIngreso From tblFacDespachoCabecera with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CNuPedido=@CNuPedido)	-- CDsEstacionIngreso
			, null
			, null
			, null
			, @NQnDespachado
			, @NVrPrecio
			, @NVrCosto
			, @NVrCosto
			, (Select IsNull(NQnBalance,0) From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select IsNull(NVtBalance,0) From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select IsNull(NQnBalance,0) From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal AND CCiBodega=@CCiBodegaPEDIDOS AND CCiUbicacion=@CCiUbicacionPEDIDOS And CCiProducto=@CCiProducto)
			, (Select IsNull(NVtBalance,0) From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal AND CCiBodega=@CCiBodegaPEDIDOS AND CCiUbicacion=@CCiUbicacionPEDIDOS And CCiProducto=@CCiProducto)
			, @CCiCia, @CCiDivision, @CCiSucursal
			, @CCiCia, @CCiDivision, @CCiSucursal;

		Select @ERRTYPE='KARDEX CARGO PEDIDO';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;


		--//*** OJO *** No borra esto se quita solo para GRUPO AR
		--//            ya que esto se acumulo en el pedido al distribuir el pedido
		----acumulo en el pedido: lo despachado
		--UPDATE tblFacDespachoDetalle
		--SET NQnDespachado=NQnDespachado+@NQnDespachado
		--WHERE CNuPedido=@prmCNuPedido
		--	  AND CCiProducto=@CCiProducto
		--	  AND NNuLinea=@NNuLinea -- pueden haber el mismo codigo + de 1 vez
		--	  AND CCiBodega=@CCiBodega
		--	  AND CCiUbicacion=@CCiUbicacion;
		--Select @ERRTYPE='DESPACHADO'
		--Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER


		--Leo el siguiente registro y obtengo el status del fetch
		FETCH NEXT FROM cur_PedidoDetalle
			INTO @CCiCia, @CCiDivision, @CCiSucursal, @CNuPedido, @CCiProducto, @NNuLinea, @NQnDespachado
			, @NVrPrecio, @NVrCosto, @NVtTotalCosto
			, @CCiBodega, @CCiUbicacion;
			Select @ERRTYPE='NEXT';
			Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

		--el fetch se carga cada vez que paso a otro registro
		SET @Fetch_Detalle=@@FETCH_STATUS
	END

CLOSE cur_PedidoDetalle
DEALLOCATE cur_PedidoDetalle
Set @InfoMessage='Cerrando detalle pedido'
Raiserror (@InfoMessage, 1, 80)

--//*** OJO *** No borrar solo se quito para GRUPO AR
--//            Ya que esto se marco al distribuir el pedido
---- si no se despacho ninguno de los items lo marco como inactivo
-- Set @InfoMessage='Verificando despacho total o parcial'
-- Raiserror (@InfoMessage, 1, 80)
-- IF (Select Sum(NQnDespachado)
--	 From tblFacDespachoDetalle
--	 Where CNuPedido=@prmCNuPedido) > 0
--	BEGIN
-- 	  Update tblFacDespachoCabecera
--	  Set CCePedido='D'
--	  Where CNuPedido=@prmCNuPedido;
--	END
-- ELSE
--	BEGIN
-- 	  --Update tblFacDespachoCabecera
--	  --Set CCePedido='I'
--	  --Where CNuPedido=@prmCNuPedido;
--	  raiserror ('El pedido no se puede despachar, no hay stock suficiente.',11,1)
--	  return 99999
--	END

-- Select @ERRTYPE='STATUS'
-- Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

 Raiserror (@InfoMessage, 1, 100)

 Raiserror ('Listo', 1, 1)

RETURN

ERR_HANDLER:
   SELECT @ERRMESSAGE=
   CASE
	 WHEN @ERRTYPE='CREATE' THEN 'Error al crear cursor.'
	 WHEN @ERRTYPE='OPEN' THEN 'Error al abrir cursor.'
	 WHEN @ERRTYPE='STATUS' THEN 'Error al cambiar status del pedido.'
	 WHEN @ERRTYPE='DESCARGO' THEN 'No se puede descargar de la bodega normal del pedido.'
	 WHEN @ERRTYPE='CARGO' THEN 'No se puede cargar a la bodega de pedidos.'
	 WHEN @ERRTYPE='NEXT' THEN 'No se pudo procesar el siguiente registro.'
	 WHEN @ERRTYPE='DESPACHADO' THEN 'No se pudo acumular en el pedido lo despachado.'
	 WHEN @ERRTYPE='KARDEX CARGO PEDIDO' THEN 'No se pudo insertar el pedido el la bodega de despachos.'
	 WHEN @ERRTYPE='KARDEX DESCARGO NORMAL' THEN 'No se pudo insertar el despacho el la bodega normal.'
   END
   RAISERROR(@ERRMESSAGE,11,1)
   RETURN @ERR
GO
