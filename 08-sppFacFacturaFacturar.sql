SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Fredy Naranjo P. veritasoft@gmail.com
-- Create date: 01-01-2010
-- Description:	Genera los procesos de facturacion
-- @CNuFactura, numero de factura que se grabo en la tabla de facturas tblFacFacturaCabecera
-- =============================================
CREATE PROCEDURE [dbo].[sppFacFacturaFacturar]
   @CCiCia					nchar(3)=null
 , @CCiDivision				nchar(3)=null
 , @CCiSucursal				nchar(3)=null
 , @prmCNuFactura			nchar(18)=Null
 , @CSNGenerarSec			nchar(1)='N'
 , @NNuSecuencia			smallint=null
 , @CSNComisionPorLinea		nchar(1)=null
 , @CDsTipoDocumento		nchar(10)=N'FACTURA'
 , @CSNGenerarSecGuia		nchar(1)=null
AS

 SET NOCOUNT ON;
 SET XACT_ABORT ON

 Declare @CCePedido nchar(1)
 Declare @CCeFactura nchar(1)

 -- variables para mensaje de informacion
 Declare @InfoMessage	nvarchar(255)
 Declare @InfoRows		int

 -- para control de errores --
 Declare @ERR		 int
 Declare @RC		 int
 Declare @ROWS		 int
 Declare @ERRTYPE	 nvarchar(20)
 Declare @ERRMESSAGE nvarchar(max)
 SET @ROWS=0


-- Declaro variables para cada registro del detalle del pedio
-- que voy a procesar
Declare @CNuFactura					nchar(18)
Declare @CNuGuia					nchar(18)
Declare @CNuPedido					nchar(18)
Declare @CCiProducto				nvarchar(20)
Declare @CCiBodega					nvarchar(8)
Declare @CCiUbicacion				nvarchar(8)
Declare @NQnExistencia				decimal(16,2)
Declare @NQnExistenciaPEDIDOS		decimal(16,2)
Declare @NQnDespachado				decimal(16,2)
Declare @NQnFacturado				decimal(16,2)
Declare @NVrPrecio					decimal(16,2)
Declare @NVrCosto					decimal(16,2)
Declare @NVtTotalCosto				decimal(16,2)
Declare @Fetch_Detalle				int

Declare @CCiBodegaPEDIDOS			nvarchar(8)
Declare @CCiUbicacionPEDIDOS		nvarchar(8)
Declare @CCiDivBodega				nchar(3)

--para secuencia de factura
Declare @NNuSec						numeric(18, 0)
Declare @CNuFacturaORIGINAL			nchar(18)
Declare @CNuFacturaNUEVA			nchar(18)
Declare @CCiCiaDivSuc				nchar(9)

Set @CNuFacturaORIGINAL=N'0'
Set @CNuFacturaNUEVA=N'0'

--para control de status del pedido
Declare @TOTALDespachado			decimal(16,2)
Declare @TOTALFacturado				decimal(16,2)


---- AQUI DEBO LEER TABLA DE PARAMETROS PARA CARGAR LA BODEGA y UBICACION de PEDIDOS
--Select @CCiBodegaPEDIDOS='0009', @CCiUbicacionPEDIDOS='01';

-- Variable para capturar la respuesta de un procedimiento externo
DECLARE @ReturnValue INT

/********************** ZONA DE ASERCION *********************************/
/* Validación de campos clave */
-- Set @InfoMessage='Validando Clave Principal'
-- Raiserror (@InfoMessage, 1, 1)
 if @CCiCia is NULL
 begin
   raiserror ('Error en campo clave CCiCia', 1, 1)
   return 99999
 end

 if @CCiDivision is NULL
 begin
   raiserror ('Error en campo clave CCiDivision', 1, 1)
   return 99999
 end

 if @CCiSucursal is NULL
 begin
   raiserror ('Error en campo clave CCiSucursal', 1, 1)
   return 99999
 end

 If @prmCNuFactura is NULL
   Begin
     Raiserror ('Error en campo clave CNuFactura.',11,1)
     Return 99999
   End

 If @NNuSecuencia is NULL
   Begin
     Raiserror ('Error en campo clave NNuSecuencia.',11,1)
     Return 99999
   End

If @CSNGenerarSec is NULL
   Begin
     Raiserror ('Error en campo clave CSNGenerarSec.',11,1)
     Return 99999
   End


/********************** ZONA DE PARAMETROS *********************************/
DECLARE	@CTxTexto							nvarchar(100)
DECLARE	@NNuvalor							Decimal(16,4)
Declare @CCiModulo							nchar(3)
Declare @tblEstados							table(CTxStatus nchar(1), CDsStatus nvarchar(50));
Declare @CTxStatusActivo					nchar(1)
Declare @CTxStatusAprobado					nchar(1)
Declare @CTxStatusDespachado				nchar(1)
Declare @CTxStatusFacturado					nchar(1)
Declare @CTxStatusCartera					nchar(1)
Declare @CTxStatusParcial					nchar(1)
Declare @CTxStatusTotal						nchar(1)
Declare @CTxStatusPedidoContabilizado		nchar(1)
Declare @CTxStatusContabilizado				nchar(1)
Declare @CCiConceptoIngresaPedido			nchar(8)
Declare @CCiTipoIngreso						nchar(1)
Declare @CCiTipoEgreso						nchar(1)
Declare @CCiTipoSec							nvarchar(10)
Declare @CCiFacturaSec						nvarchar(10)
Declare @CCiGuiaSec							nvarchar(10)
Declare @CCiTipoDocFactura					nvarchar(10)
Declare @CCiPedidoDespacho					nvarchar(10)
Declare @CCiPedidoFactura					nvarchar(10)


--// Modulo Facturacion
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'MODFAC',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno MODFAC', 1, 1)  return 99999 end
Set @CCiModulo=@CTxTexto;

--// Bodega de Pedidos Clientes
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CDFACBDCL',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDFACBDCL', 1, 1)  return 99999 end
Set @CCiBodegaPEDIDOS=@CTxTexto ;

--// Ubicacion de Pedidos Clientes
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CDFACUBCL',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDFACUBCL', 1, 1)  return 99999 end
Set @CCiUbicacionPEDIDOS=@CTxTexto;

--// Tipo Ingreso Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPINVMVIN',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPINVMVIN', 1, 1)  return 99999 end
Set @CCiTipoIngreso=@CTxTexto;

--// Tipo Egreso de Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPINVMVEG',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPINVMVEG', 1, 1)  return 99999 end
Set @CCiTipoEgreso=@CTxTexto;

--// Concepto Ingreso Pedido Inventario
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'CCFACPDIN',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CCFACPDIN', 1, 1)  return 99999 end
Set @CCiConceptoIngresaPedido=@CTxTexto;

--// Tipo Documento Despacho Facturacion
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPFACDS',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPFACDS', 1, 1)  return 99999 end
Set @CCiPedidoDespacho=@CTxTexto;

--// Tipo Documento Factura Facturacion
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = N'',
		@CCiDivision = N'',
		@CCiSucursal = N'',
		@CCiParametro = N'TPFACFC',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno TPFACFC', 1, 1)  return 99999 end
Set @CCiPedidoFactura=@CTxTexto;

--// Tipo Documento para Secuencia Factura
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = @CCiCia,
		@CCiDivision = @CCiDivision,
		@CCiSucursal = @CCiSucursal,
		@CCiParametro = N'SECFACTP',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
Set @CCiTipoSec=@CTxTexto ;

--// Secuencia para Factura
Set @CTxTexto=null ;
IF @CDsTipoDocumento=N'FACTURA'
	EXEC	[dbo].[spcConsultaParametro]
			@CCiCia = @CCiCia,
			@CCiDivision = @CCiDivision,
			@CCiSucursal = @CCiSucursal,
			@CCiParametro = N'SECFACFC',
			@CTxTexto = @CTxTexto OUTPUT,
			@NNuValor = @NNuValor OUTPUT
ELSE
	EXEC	[dbo].[spcConsultaParametro]
			@CCiCia = @CCiCia,
			@CCiDivision = @CCiDivision,
			@CCiSucursal = @CCiSucursal,
			@CCiParametro = N'SECFACNV',
			@CTxTexto = @CTxTexto OUTPUT,
			@NNuValor = @NNuValor OUTPUT ;
Set @CCiFacturaSec=@CTxTexto ;

--// Tipo de Documento para Guia de Remision
Set @CTxTexto=null ;
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = @CCiCia,
		@CCiDivision = @CCiDivision,
		@CCiSucursal = @CCiSucursal,
		@CCiParametro = N'TPFACGUI',
		@CTxTexto = @CTxTexto OUTPUT,
		@NNuValor = @NNuValor OUTPUT ;
Set @CCiGuiaSec=@CTxTexto ;

--// Codigo de Division Default para ventas
EXEC	[dbo].[spcConsultaParametro]
		@CCiCia = @CCiCia,
		@CCiDivision = @CCiDivision,
		@CCiSucursal = @CCiSucursal,
		@CCiParametro = N'CDINVDIV',
		@CTxTexto = @CTxTexto OUTPUT ,
		@NNuValor = @NNuValor OUTPUT ;
If @CTxTexto is NULL  begin raiserror ('Error en el parametro interno CDINVDIV', 1, 1)  return 99999 end
Set @CCiDivBodega=@CTxTexto;


--//Cargo tabla temporal de estados de documento
Insert Into @tblEstados EXEC [dbo].[spsFacDespachoStatusLista] @CCiCia = N'', @CCiDivision = N'', @CCiSucursal = N'', @CCiAplicacion = N'SISCO' ;
--//Estado Activo
Select @CTxStatusActivo=CTxStatus From @tblEstados WHERE CDsStatus Like N'ACTI%';
--//Estado Aprobado
Select @CTxStatusAprobado=CTxStatus From @tblEstados WHERE CDsStatus Like N'APRO%';
--//Estado Despachado
Select @CTxStatusDespachado=CTxStatus From @tblEstados WHERE CDsStatus Like N'DESP%';
--//Estado Recepcion Parcial
Select @CTxStatusParcial=CTxStatus From @tblEstados WHERE CDsStatus Like N'PARC%';
--//Estado Recepcion Total
Select @CTxStatusTotal=CTxStatus From @tblEstados WHERE CDsStatus Like N'TOTA%';
--//Estado Despacho Facturado
Select @CTxStatusFacturado=CTxStatus From @tblEstados WHERE CDsStatus Like N'FACTU%';
--//Estado Despacho Contabilizado
Select @CTxStatusPedidoContabilizado=CTxStatus From @tblEstados WHERE CDsStatus Like N'CONTA%';

----// FREDY: 2019-08-06 Verifica es status del despacho
--Select @CCePedido=CCePedido
--From tblFacDespachoCabecera  with (nolock)
--Where CCiCia = @CCiCia
--  And CCiDivision = @CCiDivision
--  And CCiSucursal = @CCiSucursal
--  And CNuPedido=@CNuFactura;

--If Not @CCePedido In (@CTxStatusActivo, @CTxStatusAprobado,@CTxStatusDespachado)
--     begin
--	   raiserror ('El despacho no se puede facturar.',11,1)
--	   return 99999
--	 end

--Raiserror (@InfoMessage, 1, 15)
-- FREDY: Hasta aqui se comentario

/********************** ZONA DE EJECUCION *********************************/

--//******* INICIO TRANSACCION ***
BEGIN TRANSACTION

----//FREDY: 2019-08-06- Genero Comision por Linea si lo amerita
--If @CSNComisionPorLinea=N'S'
--	 EXEC sppFacPedidoComisionLinea @CNuFactura, @NNuSecuencia;

----// Paso el despacho a archivo de facturas CABECERA y DETALLE
--INSERT INTO [dbo].[tblFacFacturaCabecera]
--           ( [CCiCia],[CCiDivision],[CCiSucursal]
--           , [CNuFactura],[CNuPedido],[CCiCliente],[CNoCliente]
--           , [CCiVendedor],[CNoVendedor]
--           , [CDsDireccion],[CDsTelefono],[CCeFactura]
--           , [CCiPais],[CCiProvincia],[CCiCiudad],[CCiZona]
--           , [DFxRegistro],[DFxEmision],[DFxVencimiento]
--           , [DFiIngreso],[CCiUsuarioIngreso],[CDsEstacionIngreso]
--           , [DFmModifica],[CCiUsuarioModifica],[CDsEstacionModifica]
--           , [CCiBodega],[CNoBodega],[CCiUbicacion],[CNoUbicacion]
--           , [NVtSubtotal],[NVrPorcDescto],[NVtDescto],[NVtIvaCero]
--           , [NVrPorcIva],[NVtIva],[NVtTotal],[NVtTotalCosto]
--           , [NNuPrecio],[BSNSeparado],[CCiGrupo],[CNoGrupo]
--           , [CDsRuc],[CCiPolitica],[CNuReferencia],[DFxAprobacion]
--           , [CCiFormaPago],[CNoFormaPago]
--           , [CCiTransporte],[CNoTransporte]
--           , [CNuPlazo],[CCiZonaVenta],[CNoZonaVenta]
--           , [CCiGestorVenta],[CNoGestorVenta],[CDsObservacion]
--           , [NVtTotalDescto],[NVrPorcTransporte],[NVtTransporte]
--           , [NVrPorcComision],[NVtComision],[CCiDireccion],[CNoDireccion]
--           , [CCiNegocio], [CNoRazon], [NVtSubtotalCompra], [NVtSubtotal2], [NVtDesctoLinea], [NNuSecuencia])
--     SELECT
--             [CCiCia],[CCiDivision],[CCiSucursal]
--           , [CNuPedido],[CNuPedido],[CCiCliente],[CNoCliente]
--           , [CCiVendedor],[CNoVendedor]
--           , [CDsDireccion],[CDsTelefono], @CTxStatusAprobado
--           , [CCiPais],[CCiProvincia],[CCiCiudad],[CCiZona]
--           , convert(datetime, convert(nvarchar(11),[DFxEmision],105) + ' ' +  convert(nvarchar(8),getdate(),108), 105)
--           , convert(datetime, convert(nvarchar(11),[DFxEmision],105) + ' ' +  convert(nvarchar(8),getdate(),108), 105)
--           , [DFxVencimiento]
--           , convert(datetime, convert(nvarchar(11),[DFxEmision],105) + ' ' +  convert(nvarchar(8),getdate(),108), 105)
--           , [CCiUsuarioIngreso],[CDsEstacionIngreso]
--           , [DFmModifica],[CCiUsuarioModifica],[CDsEstacionModifica]
--           , [CCiBodega],[CNoBodega],[CCiUbicacion],[CNoUbicacion]
--           , [NVtSubtotal],[NVrPorcDescto],[NVtDescto],[NVtIvaCero]
--           , [NVrPorcIva],[NVtIva],[NVtTotal],[NVtTotalCosto]
--           , [NNuPrecio],[BSNSeparado],[CCiGrupo],[CNoGrupo]
--           , [CDsRuc],[CCiPolitica],[CNuReferencia],[DFxAprobacion]
--           , [CCiFormaPago],[CNoFormaPago]
--           , [CCiTransporte],[CNoTransporte]
--           , [CNuPlazo],[CCiZonaVenta],[CNoZonaVenta]
--           , [CCiGestorVenta],[CNoGestorVenta],[CDsObservacion]
--           , [NVtTotalDescto],[NVrPorcTransporte],[NVtTransporte]
--           , [NVrPorcComision],[NVtComision],[CCiDireccion],[CNoDireccion]
--           , [CCiNegocio], [CNoRazon]
--           , [NVtSubtotalCompra], [NVtSubtotal2], [NVtDesctoLinea], [NNuSecuencia]
--     FROM [dbo].[tblFacDespachoCabecera]  with (nolock)
--     WHERE CCiCia = @CCiCia
--	   And CCiDivision = @CCiDivision
--	   And CCiSucursal = @CCiSucursal
--	   And CNuPedido = @CNuFactura
--	   And NNuSecuencia = @NNuSecuencia;

--	--DETALLE
--	INSERT INTO [dbo].[tblFacFacturaDetalle]
--			   ([CCiCia],[CCiDivision],[CCiSucursal]
--			   ,[CNuFactura],[CNuPedido],[CCiCliente]
--			   ,[CCiProducto],[NNuLinea],[CNoProducto]
--			   ,[NQnPedido],[NQnDespachado],[NQnFacturado]
--			   ,[NNuInnerBox],[NNuOuterBox],[NVrPrecio],[NVrCosto]
--			   ,[NVtSubtotal1],[NVtSubtotal2],[CSNIva],[NVtIvaCero]
--			   ,[NVrPorcIva],[NVtIva],[NVtTotal],[NVtTotalCosto]
--			   ,[CCiClasificacion1],[CCiClasificacion2],[CCiClasificacion3]
--			   ,[CCiBodega],[CCiUbicacion],[CNuReferencia],[CCeFactura],[CCiProveedor]
--			   ,[NVrPorcDescto1],[NVtDescto1],[NVrPorcDescto2],[NVtDescto2],[NVrPorcDescto3],[NVtDescto3]
--			   ,[NVrPorcTransporte],[NVtTransporte]
--			   ,[NVrPorcComision],[NVtComision], [NNuPrecio], [NNuSecuencia])
--	SELECT  [CCiCia],[CCiDivision],[CCiSucursal]
--           ,[CNuPedido],[CNuPedido],[CCiCliente]
--           ,[CCiProducto],[NNuLinea],[CNoProducto]
--           ,[NQnPedido],[NQnDespachado],[NQnDespachado]
--           ,[NNuInnerBox],[NNuOuterBox],[NVrPrecio],[NVrCosto]
--           ,[NVtSubtotal1],[NVtSubtotal2],[CSNIva],[NVtIvaCero]
--           ,[NVrPorcIva],[NVtIva],[NVtTotal],[NVtTotalCosto]
--           ,[CCiClasificacion1],[CCiClasificacion2],[CCiClasificacion3]
--           ,[CCiBodega],[CCiUbicacion],[CNuReferencia],[CCePedido],[CCiProveedor]
--           ,[NVrPorcDescto1],[NVtDescto1],[NVrPorcDescto2],[NVtDescto2],[NVrPorcDescto3],[NVtDescto3]
--           ,[NVrPorcTransporte],[NVtTransporte]
--           ,[NVrPorcComision],[NVtComision],[NNuPrecio],[NNuSecuencia]
--    FROM [dbo].[tblFacDespachoDetalle]  with (nolock)
--    WHERE CCiCia = @CCiCia
--	  And CCiDivision = @CCiDivision
--	  And CCiSucursal = @CCiSucursal
--	  And CNuPedido = @CNuFactura
--	  And NNuSecuencia = @NNuSecuencia;

-- -- ////////////// OBTENER SECUENCIA DE LA FACTURA ////////////////
-- -- Set @InfoMessage='Obteniendo secuencia de factura y reemplazando el numero temporal'
-- -- Raiserror (@InfoMessage, 1, 15)

-- Set @CNuFacturaORIGINAL = @CNuFactura;
-- Set @CCiCiaDivSuc = rtrim(ltrim(@CCiCia)) + rtrim(ltrim(@CCiDivision)) + rtrim(ltrim(@CCiSucursal));

-- IF @CSNGenerarSec=N'S' --//Hay que generar numero de factura y guia de remision
--   Begin
--	 --//FACTURA
--	 EXEC	@ReturnValue = [dbo].[sppGenConsecutivoGeneral]
--			@Consecutivo = @CCiTipoSec,
--			@CCiCiaDivSuc = @CCiCiaDivSuc,
--			@CCiTipoConsec = @CCiFacturaSec,
--			@BRollBack = 0,
--			@BSoloConsulta = 0,
--			@NNuSec = @NNuSec OUTPUT;

--	 If @ReturnValue=-1
--	   Begin
--		Select @ERRTYPE='SECUENCE';
--		GOTO ERR_HANDLER;
--	   End

--	 Set @CNuFacturaNUEVA=Cast(@NNuSec As nchar(18));


--     Set @CNuGuia=Cast(0 As nchar(18));
--	 IF @CSNGenerarSecGuia=N'S'
--	   BEGIN
--		 --//GUIA DE REMISION
--		 EXEC	@ReturnValue = [dbo].[sppGenConsecutivoGeneral]
--				@Consecutivo = @CCiTipoSec,
--				@CCiCiaDivSuc = @CCiCiaDivSuc,
--				@CCiTipoConsec = @CCiGuiaSec,
--				@BRollBack = 0,
--				@BSoloConsulta = 0,
--				@NNuSec = @NNuSec OUTPUT;

--		 If @ReturnValue=-1
--		   Begin
--			Select @ERRTYPE='SECUENCE';
--			GOTO ERR_HANDLER;
--		   End

--		 Set @CNuGuia=Cast(@NNuSec As nchar(18));
--	   END

--   End
--ELSE  --//No generar numero de factura (conservar el numero enviado como parametro)
--   Begin
--     Set @CNuFacturaNUEVA=@CNuFacturaORIGINAL ;
--   End

-- Set @CNuFactura=N'';
-- Set @CNuFactura=@CNuFacturaNUEVA;

-- --//ACTUALIZO EL NUMERO DE FACTURA y GUIA
-- UPDATE tblFacFacturaCabecera
-- Set CNuFactura=@CNuFacturaNUEVA
--   , CNuGuia=@CNuGuia
-- Where CCiCia = @CCicia
--   And CCiDivision = @CCiDivision
--   And CCiSucursal = @CCiSucursal
--   And CNuFactura = @CNuFacturaORIGINAL ;

-- Select @ERRTYPE='SECUENCEUPDATE';
-- Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;
-- Raiserror (@InfoMessage, 1, 30)
-- -- /////////////////////////////////////////////////

-- FREDY 2019-08-06 : Hasta aqui comente

Declare @NNuLinea		int;
Set @NNuLinea=0;

--Declaro Cursor
DECLARE cur_FacturaDetalle INSENSITIVE CURSOR FOR
	Select CNuFactura, CNuPedido, CCiProducto, NQnDespachado, NQnFacturado
	     , NVrPrecio, NVrCosto, NVtTotalCosto
		 , CCiBodega, CCiUbicacion
	From
	    tblFacFacturaDetalle
	Where
	      CCiCia = @CCiCia
	  And CCiDivision = @CCiDivision
	  And CCiSucursal = @CCiSucursal
	  And CNuFactura = @prmCNuFactura AND NQnFacturado>0;
	Select @ERRTYPE='CREATE';
	Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

--Abro el cursor
OPEN cur_FacturaDetalle

--Leo el primer registro y obtengo el status del fetch
FETCH NEXT FROM cur_FacturaDetalle
	INTO @CNuFactura, @CNuPedido, @CCiProducto, @NQnDespachado, @NQnFacturado
  	   , @NVrPrecio, @NVrCosto, @NVtTotalCosto
	   , @CCiBodega, @CCiUbicacion;
	Select @ERRTYPE='OPEN';
	Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

--el fetch se carga cada vez que paso a otro registro
SET @Fetch_Detalle=@@FETCH_STATUS

--ejecuto este bloque mientras el fetch sea diferente de -1
--es -1 cuando no hay datos en el cursor o se alcanza el fin del cursor
WHILE @Fetch_Detalle <> -1
	BEGIN
		Set @NNuLinea=@NNuLinea+1;

		-- verifico la existencia de stock
	    Set @NQnExistencia=0.00;

		--existencia bodega NORMAL
		Select @NQnExistencia = NQnExistencia
		From tblInvSaldoBodega  with (nolock)
		WHERE CCiCia = @CCiCia
		  And CCiDivision = @CCiDivBodega
		  And CCiSucursal = @CCiSucursal
		  AND CCiBodega=@CCiBodega
		  AND CCiUbicacion=@CCiUbicacion
  		  And CCiProducto=@CCiProducto;
		Select @ERRTYPE='EXISTENCIA';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

		-- existencia bodega PEDIDOS
	    Set @NQnExistenciaPEDIDOS=0.00;
		Select @NQnExistenciaPEDIDOS = NQnExistencia
		From tblInvSaldoBodega  with (nolock)
		WHERE CCiCia = @CCiCia
		  And CCiDivision = @CCiDivBodega
		  And CCiSucursal = @CCiSucursal
		  AND CCiBodega=@CCiBodegaPEDIDOS
		  AND CCiUbicacion=@CCiUbicacionPEDIDOS
  		  And CCiProducto=@CCiProducto;
		Select @ERRTYPE='EXISTENCIA';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

--		If @CNuPedido<>0 -- si procesa CON PEDIDO, verifico stock en bodega pedidos
--			BEGIN
--			    If ((@NQnFacturado) > (@NQnExistencia + @NQnExistenciaPEDIDOS))
--				    BEGIN
--				       CLOSE cur_FacturaDetalle
--				       DEALLOCATE cur_FacturaDetalle
--				       Set @ERRMESSAGE='No hay stock suficiente para el item: ' + @CCiProducto + ' , Facturado: ' + convert(nvarchar, @NQnFacturado) + ', Stock: ' + convert(nvarchar, @NQnExistencia);
--				       Raiserror (@ERRMESSAGE ,11,1);
--				       Return 99999;
--				    END
--			END
--		Else			-- procesa sin pedido, FACTURA DIRECTA
--			BEGIN
--			    If ((@NQnFacturado) > (@NQnExistencia))
--				    BEGIN
--				       CLOSE cur_FacturaDetalle
--				       DEALLOCATE cur_FacturaDetalle
--				       Set @ERRMESSAGE='No hay stock suficiente para el item: ' + @CCiProducto + ' , Facturado: ' + convert(nvarchar, @NQnFacturado) + ', Stock: ' + convert(nvarchar, @NQnExistencia);
--				       Raiserror (@ERRMESSAGE ,11,1);
--				       Return 99999;
--				    END
--			END

--******************* QUITE DESDE AQUI PARA QUIMISER PARA NO REGISTRAR MOVIMIENTOS DE RETORNO DE STOCK DESDE BODEGA CLIENTES A BODEGA NORMAL *************
		----//si procesa CON PEDIDO
		--If @CNuPedido<>0
		--	BEGIN

----////////////////////////////////////// SACA DE BODEGA DE PEDIDOS ////////////////////////////////////////
--				-- descargo la bodega pedido (REVERSO DESPACHO)
--				-- actualizo PRODUCTO
--				UPDATE tblInvPRODUCTO
--				SET NQnBalance=(NQnBalance-@NQnDespachado)
--				  , NVtBalance=(NVtBalance-(@NQnDespachado*@NVrCosto))
--				WHERE CCiProducto=@CCiProducto;
--				Select @ERRTYPE='DESCARGO';
--				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER;

--				-- actualizo BODEGA PEDIDOS
--				-- *** ESTO ES SOLO SI QUISIERA CONSERVAR SALDOS DE PEDIDOS PARA
--				-- *** DESPACHOS PARCIALES
----			    IF ((@NQnExistenciaPEDIDOS-@NQnFacturado) < 0)
----				    BEGIN
----					  UPDATE tblInvSaldoBODEGA
----					  SET NQnExistencia=NQnExistencia-@NQnDespachado
----					  , NQnBalance=NQnBalance-@NQnDespachado
----					  , NVtBalance=NVtBalance-(@NQnDespachado*@NVrCosto)
----					  WHERE CCiProducto=@CCiProducto
----				  		  AND CCiBodega=@CCiBodegaPEDIDOS
----						  AND CCiUbicacion=@CCiUbicacionPEDIDOS;
----
----				  	  Select @ERRTYPE='DESCARGO';
----					  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
----				    END
----				ELSE
----				    BEGIN
----					  UPDATE tblInvSaldoBODEGA
----					  SET NQnExistencia=NQnExistencia-@NQnFacturado
----					  , NQnBalance=NQnBalance-@NQnFacturado
----					  , NVtBalance=NVtBalance-(@NQnFacturado*@NVrCosto)
----					  WHERE CCiProducto=@CCiProducto
----				  		  AND CCiBodega=@CCiBodegaPEDIDOS
----						  AND CCiUbicacion=@CCiUbicacionPEDIDOS;
----
----				  	  Select @ERRTYPE='DESCARGO';
----					  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
----				    END

--			  -- *** REVERSA EL PEDIDO COMPLETO SIN DEJAR PARA DESPACHOS PARCIALES
--			    UPDATE tblInvSaldoBODEGA
--			    SET NQnExistencia = ( NQnExistencia - @NQnDespachado )
--			      , NQnBalance = ( NQnBalance - @NQnDespachado )
--			      , NVtBalance = ( NVtBalance - ( @NQnDespachado * @NVrCosto ) )
--			    WHERE CCiCia = @CCiCia
--				  And CCiDivision = @CCiDivBodega
--				  And CCiSucursal = @CCiSucursal
--				  AND CCiBodega=@CCiBodegaPEDIDOS
--				  AND CCiUbicacion=@CCiUbicacionPEDIDOS
--  				  And CCiProducto=@CCiProducto;
--		  	    Select @ERRTYPE='DESCARGO PEDIDOS';
--			    Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

--				IF @NQnDespachado > 0
--					Begin
--						/** AQUI VA LA PARTE PARA INSERTAR EL MOVIMIENTO EN EL KARDEX: Descargo bodega de pedidos **/
--						INSERT INTO tblInvKardex
--						(
--							  NNuLinea
--							, CCiProducto
--							, CCiBodegaOrigen
--							, CCiUbicacionOrigen
--							, CCiBodegaDestino
--							, CCiUbicacionDestino
--							, CCiTipoMov
--							, CCiConcepto
--							, CNuMovimiento
--							, CNuReferencia
--							, DFxRegistro
--							, DFiIngreso
--							, CCiUsuarioIngreso
--							, CDsEstacionIngreso
--							, DFmModifica
--							, CCiUsuarioModifica
--							, CDsEstacionModifica
--							, NQnCantidad
--							, NVrPrecio
--							, NVrCosto
--							, NVrNuevoCosto
--							, NQnBalanceGen
--							, NVtBalanceGen
--							, NQnBalanceParc
--							, NVtBalanceParc
--							, CCiCia, CCiDivision, CCiSucursal
--							, CCiCia2, CCiDivision2, CCiSucursal2
--						)
--						Select
--							  @NNuLinea
--							, @CCiProducto
--							, @CCiBodegaPEDIDOS
--							, @CCiUbicacionPEDIDOS
--							, @CCiBodega
--							, @CCiUbicacion
--							, @CCiTipoEgreso		-- CCiTipo
--							, @CCiPedidoDespacho	-- CCiMovimiento
--							, @CNuPedido			-- CNuMovimiento
--							, @CNuFactura			--(Select CCiReferencia From tblFacPedidoCabecera Where CNuPedido=@CNuPedido)	-- CNuReferencia
--							, (Select DFxEmision From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)
--							, (Select DFiIngreso From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)			-- getdate()
--							, (Select CCiUsuarioIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)	-- CCiUsuarioIngreso
--							, (Select CDsEstacionIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)	-- CDsEstacionIngreso
--							, null
--							, null
--							, null
--							, (@NQnDespachado*(-1)) -- *** OJO: ESTO SERIA PARA DESPACHOS PARCIALES *** CASE WHEN ((@NQnExistenciaPEDIDOS-@NQnFacturado) < 0) THEN (@NQnDespachado*(-1)) ELSE (@NQnFacturado*(-1)) END
--							, @NVrPrecio
--							, @NVrCosto
--							, @NVrCosto
--							, (Select NQnBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
--							, (Select NVtBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
--							, (Select NQnBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CCiProducto=@CCiProducto AND CCiBodega=@CCiBodegaPEDIDOS AND CCiUbicacion=@CCiUbicacionPEDIDOS)
--							, (Select NVtBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia = @CCiCia And CCiDivision = @CCiDivision And CCiSucursal = @CCiSucursal And CCiProducto=@CCiProducto AND CCiBodega=@CCiBodegaPEDIDOS AND CCiUbicacion=@CCiUbicacionPEDIDOS)
--							, @CCiCia, @CCiDivBodega, @CCiSucursal
--							, @CCiCia, @CCiDivBodega, @CCiSucursal;

--						Select @ERRTYPE='KARDEX DESCARGO PEDIDO';
--						Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
--					End

----///////////////////////////////ENTRA A BODEGA NORMAL ///////////////////////////////////////////////////

--				-- cargo la bodega normal (REVERSO DESPACHO)
--				-- actualizo PRODUCTO
--				UPDATE tblInvPRODUCTO
--				SET NQnBalance=(NQnBalance+@NQnDespachado)
--				  , NVtBalance=(NVtBalance+(@NQnDespachado*@NVrCosto))
--				WHERE CCiProducto=@CCiProducto;
--				Select @ERRTYPE='CARGO'
--				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

--				-- actualizo BODEGA
--				-- *** ESTO ES SOLO SI QUISIERA CONSERVAR SALDOS DE PEDIDOS PARA
--				-- *** DESPACHOS PARCIALES
----			    IF ((@NQnExistenciaPEDIDOS-@NQnFacturado) < 0)
----				    BEGIN
----					  UPDATE tblInvSaldoBODEGA
----					  SET NQnExistencia=NQnExistencia+@NQnDespachado
----					  , NQnBalance=NQnBalance+@NQnDespachado
----				  	  , NVtBalance=NVtBalance+(@NQnDespachado*@NVrCosto)
----					  WHERE CCiProducto=@CCiProducto
----						  AND CCiBodega=@CCiBodega
----						  AND CCiUbicacion=@CCiUbicacion;
----					  Select @ERRTYPE='CARGO';
----					  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
----				    END
----				ELSE
----				    BEGIN
----					  UPDATE tblInvSaldoBODEGA
----					  SET NQnExistencia=NQnExistencia+@NQnFacturado
----					  , NQnBalance=NQnBalance+@NQnFacturado
----				  	  , NVtBalance=NVtBalance+(@NQnFacturado*@NVrCosto)
----					  WHERE CCiProducto=@CCiProducto
----						  AND CCiBodega=@CCiBodega
----						  AND CCiUbicacion=@CCiUbicacion;
----					  Select @ERRTYPE='CARGO';
----					  Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
----				    END

--			  -- *** REVERSA EL PEDIDO COMPLETO SIN DEJAR PARA DESPACHOS PARCIALES
--			    UPDATE tblInvSaldoBODEGA
--			    SET NQnExistencia=(NQnExistencia+@NQnDespachado)
--			      , NQnBalance=(NQnBalance+@NQnDespachado)
--		  	      , NVtBalance=(NVtBalance+(@NQnDespachado*@NVrCosto))
--			    WHERE CCiCia = @CCiCia
--				  And CCiDivision = @CCiDivBodega
--				  And CCiSucursal = @CCiSucursal
--				  AND CCiBodega = @CCiBodega
--				  AND CCiUbicacion = @CCiUbicacion
--  				  And CCiProducto = @CCiProducto ;
--			    Select @ERRTYPE='CARGO NORMAL';
--			    Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

--				IF @NQnDespachado>0
--					Begin
--						/** AQUI VA LA PARTE PARA INSERTAR EL MOVIMIENTO EN EL KARDEX: Cargo bodega normal **/
--						INSERT INTO tblInvKardex
--						(
--							  NNuLinea
--							, CCiProducto
--							, CCiBodegaOrigen
--							, CCiUbicacionOrigen
--							, CCiBodegaDestino
--							, CCiUbicacionDestino
--							, CCiTipoMov
--							, CCiConcepto
--							, CNuMovimiento
--							, CNuReferencia
--							, DFxRegistro
--							, DFiIngreso
--							, CCiUsuarioIngreso
--							, CDsEstacionIngreso
--							, DFmModifica
--							, CCiUsuarioModifica
--							, CDsEstacionModifica
--							, NQnCantidad
--							, NVrPrecio
--							, NVrCosto
--							, NVrNuevoCosto
--							, NQnBalanceGen
--							, NVtBalanceGen
--							, NQnBalanceParc
--							, NVtBalanceParc
--							, CCiCia, CCiDivision, CCiSucursal
--							, CCiCia2, CCiDivision2, CCiSucursal2
--						)
--						Select
--							  @NNuLinea
--							, @CCiProducto
--							, @CCiBodega
--							, @CCiUbicacion
--							, @CCiBodegaPEDIDOS
--							, @CCiUbicacionPEDIDOS
--							, @CCiTipoIngreso		-- CCiTipo
--							, @CCiPedidoDespacho	-- CCiMovimiento
--							, @CNuPedido	-- CNuMovimiento
--							, @CNuFactura	--(Select CCiReferencia From tblFacPedidoCabecera Where CNuPedido=@CNuPedido)	-- CNuReferencia
--							, (Select DFxEmision From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)
--							, (Select DFiIngreso From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)			-- getdate()
--							, (Select CCiUsuarioIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)		-- CCiUsuarioIngreso
--							, (Select CDsEstacionIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)	-- CDsEstacionIngreso
--							, null
--							, null
--							, null
--							, @NQnDespachado -- *** OJO: ESTO SERIA PARA DESPACHOS PARCIALES *** CASE WHEN ((@NQnExistenciaPEDIDOS-@NQnFacturado) < 0) THEN @NQnDespachado ELSE @NQnFacturado END
--							, @NVrPrecio
--							, @NVrCosto
--							, @NVrCosto
--							, (Select NQnBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
--							, (Select NVtBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
--							, (Select NQnBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion And CCiProducto=@CCiProducto )
--							, (Select NVtBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion And CCiProducto=@CCiProducto )
--							, @CCiCia, @CCiDivBodega, @CCiSucursal
--							, @CCiCia, @CCiDivBodega, @CCiSucursal ;

--						Select @ERRTYPE='KARDEX CARGO NORMAL';
--						Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
--					End
--			END
--.......... QUITE HASTA AQUI PARA QUIMISER




--///////////////////////////////// VENTA NORMAL ////////////////////////////////////////////////////

		-- descargo la bodega normal (VENTA)
		-- actualizo PRODUCTO
		UPDATE tblInvPRODUCTO
		SET NQnBalance=NQnBalance-@NQnFacturado
		  , NVtBalance=NVtBalance-(@NQnFacturado*@NVrCosto)
		WHERE CCiProducto=@CCiProducto;
		Select @ERRTYPE='DESCARGO'
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

		-- actualizo BODEGA
		UPDATE tblInvSaldoBodega
		SET NQnExistencia=NQnExistencia-@NQnFacturado
		  , NQnBalance=NQnBalance-@NQnFacturado
		  , NVtBalance=NVtBalance-(@NQnFacturado*@NVrCosto)
		WHERE CCiCia = @CCiCia
		  And CCiDivision = @CCiDivBodega
		  And CCiSucursal = @CCiSucursal
		  AND CCiBodega=@CCiBodega
		  AND CCiUbicacion=@CCiUbicacion
		  And CCiProducto=@CCiProducto ;

		Select @ERRTYPE='DESCARGO';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

		--***** aqui debo poner los movimientos de KARDEX para SACAR DE BODEGA NORMAL (FACTURA)***
		/** AQUI VA LA PARTE PARA INSERTAR EL MOVIMIENTO EN EL KARDEX: Descargo bodega normal **/
		INSERT INTO tblInvKardex
		(
			  NNuLinea
			, CCiProducto
			, CCiBodegaOrigen
			, CCiUbicacionOrigen
			, CCiBodegaDestino
			, CCiUbicacionDestino
			, CCiTipoMov
			, CCiConcepto
			, CNuMovimiento
			, CNuReferencia
			, DFxRegistro
			, DFiIngreso
			, CCiUsuarioIngreso
			, CDsEstacionIngreso
			, DFmModifica
			, CCiUsuarioModifica
			, CDsEstacionModifica
			, NQnCantidad
			, NVrPrecio
			, NVrCosto
			, NVrNuevoCosto
			, NQnBalanceGen
			, NVtBalanceGen
			, NQnBalanceParc
			, NVtBalanceParc
			, CCiCia, CCiDivision, CCiSucursal
			, CCiCia2, CCiDivision2, CCiSucursal2
		)
		Select
			  @NNuLinea
			, @CCiProducto
			, @CCiBodega
			, @CCiUbicacion
			, @CCiBodega
			, @CCiUbicacion
			, @CCiTipoEgreso		-- CCiTipo
			, @CCiPedidoFactura		-- CCiMovimiento
			, @CNuFactura	-- CNuMovimiento
			, @CNuPedido	--(Select CCiReferencia From tblFacPedidoCabecera Where CNuPedido=@CNuPedido)	-- CNuReferencia
			, (Select DFxEmision From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)
			, (Select DFiIngreso From tblFacFacturaCabecera Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)			-- getdate()
			, (Select CCiUsuarioIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)		-- CCiUsuarioIngreso
			, (Select CDsEstacionIngreso From tblFacFacturaCabecera with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal And CNuFactura=@CNuFactura)	-- CDsEstacionIngreso
			, null
			, null
			, null
			, (@NQnFacturado*(-1))
			, @NVrPrecio
			, @NVrCosto
			, @NVrCosto
			, (Select NQnBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select NVtBalance From tblInvPRODUCTO with (nolock) Where CCiProducto=@CCiProducto)
			, (Select NQnBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion AND CCiProducto=@CCiProducto)
			, (Select NVtBalance From tblInvSALDOBODEGA with (nolock) Where CCiCia=@CCiCia And CCiDivision=@CCiDivision And CCiSucursal=@CCiSucursal AND CCiBodega=@CCiBodega AND CCiUbicacion=@CCiUbicacion And CCiProducto=@CCiProducto )
			, @CCiCia, @CCiDivBodega, @CCiSucursal
			, @CCiCia, @CCiDivBodega, @CCiSucursal ;

		Select @ERRTYPE='KARDEX FACTURA';
		Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;



		--//*** OJO *** No borrar solo se quita esto para Grupo AR
		/*
		If @CNuPedido<>0 -- si procesa en base a pedido
			BEGIN
				--acumulo en el pedido: lo facturado
				UPDATE tblFacPedidoDetalle
				SET NQnFacturado=NQnFacturado+@NQnFacturado
				WHERE CNuPedido=@CNuPedido
					  AND CCiProducto=@CCiProducto
					  AND CCiBodega=@CCiBodega
					  AND CCiUbicacion=@CCiUbicacion;

				Select @ERRTYPE='FACTURADO';
				Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;
			END

		... */

		--Leo el siguiente registro y obtengo el status del fetch
		FETCH NEXT FROM cur_FacturaDetalle
			INTO @CNuFactura, @CNuPedido, @CCiProducto, @NQnDespachado, @NQnFacturado
			   , @NVrPrecio, @NVrCosto, @NVtTotalCosto
			   , @CCiBodega, @CCiUbicacion;
			Select @ERRTYPE='NEXT';
			Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER ;

		--el fetch se carga cada vez que paso a otro registro
		SET @Fetch_Detalle=@@FETCH_STATUS
	END

CLOSE cur_FacturaDetalle
DEALLOCATE cur_FacturaDetalle


 ----//FREDY: 2019-08-06 Cambiar estado del despacho a facturado
 --UPDATE [dbo].[tblFacDespachoCabecera]
 --SET CCePedido=@CTxStatusFacturado
 --WHERE CCiCia = @CCiCia
 --  And CCiDivision = @CCiDivision
 --  And CCiSucursal = @CCiSucursal
 --  And CNuPedido = @CNuFactura
 --  And NNuSecuencia = @NNuSecuencia;

 -- FREDY: Hasta aqui el comentario


 --//*** OJO *** No borrar solo se quita esto para Grupo AR
 /*
 -- verifico si se recibio todo el pedido para marcar como RECIBIDO TOTAL (T), sino (R) parcial
 Set @TOTALDespachado = 0.00
 Set @TOTALFacturado  = 0.00

 Select @TOTALDespachado=Sum(NQnDespachado)
 From tblFacPedidoDetalle
 Where CNuPedido=@CNuPedido;
 Select @ERRTYPE='STATUS'
 Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

 Select @TOTALFacturado=Sum(NQnFacturado)
 From tblFacFacturaDetalle
 Where CNuPedido=@CNuPedido;
 Select @ERRTYPE='STATUS'
 Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

 --IF @CNuPedido<>0
 --  BEGIN
		 If @TOTALFacturado>=@TOTALDespachado
			BEGIN
			 Update tblFacPedidoCabecera
			 Set CCePedido='T'
			 Where CNuPedido=@CNuPedido;
			END
		 Else
			BEGIN
			 Update tblFacPedidoCabecera
			 Set CCePedido='R'
			 Where CNuPedido=@CNuPedido;
			END
		 Select @ERRTYPE='STATUS'
		 Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER
--	END
... */

 Raiserror (@InfoMessage, 1, 80)

 -- FREDY: 2019-08-06 -- Ejecuto el procedimiento para grabar la factura en la cartera
 ---- si el procedimiento falla emito un mensaje y salgo con error
 --Set @InfoMessage='Pasando la Factura a Cartera'
 --Raiserror (@InfoMessage, 1, 80)
 --EXEC @ReturnValue = [dbo].[sppFacFacturaCartera]
	--  @CCiCia
	--, @CCiDivision
	--, @CCiSucursal
	--, @CNuFacturaNUEVA;
 --IF @ReturnValue<0
	--BEGIN
	-- Select @ERRTYPE='CARTERA'
	-- GOTO ERR_HANDLER
	--END

 --Set @InfoMessage='Calculando Comisones'
 --Raiserror (@InfoMessage, 1, 90)
 --EXEC @ReturnValue = [dbo].[sppFacComisionPorcentajes]
 --     @CNuFacturaNUEVA ;
 --IF @ReturnValue<0
	--BEGIN
	-- Select @ERRTYPE='COMISION'
	-- GOTO ERR_HANDLER
	--END

 -- FREDY: Hasta aqui comente


 Raiserror (@InfoMessage, 1, 100)

 Raiserror ('Listo...', 1, 1)


 --// ****** TERMINO TRANSACCION
 COMMIT TRANSACTION

 RETURN 0

ERR_HANDLER:

   SELECT @ERRMESSAGE=
   CASE
	 WHEN @ERRTYPE='CREATE' THEN 'Error al crear cursor.'
	 WHEN @ERRTYPE='OPEN' THEN 'Error al abrir cursor.'
	 WHEN @ERRTYPE='STATUS' THEN 'Error al cambiar status del pedido.'
	 WHEN @ERRTYPE='EXISTENCIA' THEN 'No se obtener la existencia del item actual.'
	 WHEN @ERRTYPE='DESCARGO PEDIDOS' THEN 'No se puede descargar de la bodega normal del pedido.'
	 WHEN @ERRTYPE='CARGO NORMAL' THEN 'No se puede cargar a la bodega de pedidos.'
	 WHEN @ERRTYPE='NEXT' THEN 'No se pudo procesar el siguiente registro.'
	 WHEN @ERRTYPE='FACTURADO' THEN 'No se pudo acumular en el pedido lo facturado.'
	 WHEN @ERRTYPE='CARTERA' THEN 'No se pudo enviar la factura a cartera.'
	 WHEN @ERRTYPE='KARDEX DESCARGO PEDIDO' THEN 'No se pudo insertar el despacho en el kardex.'
	 WHEN @ERRTYPE='KARDEX CARGO NORMAL' THEN 'No se pudo insertar el reverso de despacho en el kardex.'
	 WHEN @ERRTYPE='KARDEX FACTURA' THEN 'No se pudo insertar la factura en el kardex.'
	 WHEN @ERRTYPE='REVERSE' THEN 'Error al reversar el numero de factura.'
	 WHEN @ERRTYPE='SECUENCE' THEN 'Error al obtener el numero de factura.'
	 WHEN @ERRTYPE='SECUENCEUPDATE' THEN 'Error al poner el numero de factura.'
	 WHEN @ERRTYPE='CARTERA' THEN 'No enviar la factura a cartera.<MSG>'
	 WHEN @ERRTYPE='COMISION' THEN 'No se pudo colocar comisiones en la factura.<MSG>'
   END

   --// ****** REVERSO TRANSACCION
   ROLLBACK TRANSACTION

   -- Reversar el numero de la factura al numero aleatorio
   UPDATE tblFacFacturaCabecera
   Set CNuFactura=@CNuFacturaORIGINAL
   Where CNuFactura=@CNuFacturaNUEVA
   --Select @ERRTYPE='REVERSE'
   --Select @ERR=@@ERROR, @RC=@@ROWCOUNT; IF @ERR != 0  GOTO ERR_HANDLER

   RAISERROR(@ERRMESSAGE,11,1);

   RETURN @ERR;
GO
